package dice.assembleguns.component;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import dice.assembleguns.component.components.GunComponents;
import dice.assembleguns.component.components.GunComponentsManager;
import dice.assembleguns.items.AssembleGunItem;
import dice.assembleguns.packets.ColourMessage;
import dice.assembleguns.packets.ModPackets;
import dice.assembleguns.packets.ModifyMessage;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.play.client.CRenameItemPacket;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.util.text.*;
import pellucid.ava.misc.AVAConstants;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class
ModificationScreen extends Screen
{
    private static final ResourceLocation TEXTURE = new ResourceLocation("assemble_guns:textures/gui/modification_gui.png");
    private static final int TEXTURE_WINDOW_WIDTH = 256;
    private static final int TEXTURE_WINDOW_HEIGHT = 224;
    private int colour = BackgroundColours.BLACK.colour;
    protected int guiLeft;
    protected int guiTop;
    protected GunParts selectedCategory = GunParts.HANDGUARD;
    protected TextFieldWidget textField;

    public ModificationScreen()
    {
        super(new StringTextComponent(""));
    }

    @Override
    protected void init()
    {
        guiLeft = (width - TEXTURE_WINDOW_WIDTH) / 2;
        guiTop = (height - TEXTURE_WINDOW_HEIGHT) / 2;
        for (int i = 0; i < BackgroundColours.values().length; i++)
            addButton(new BackgroundColourButton(guiLeft + 18 + i * 5, guiTop + 13, i));
        for (int i = 3; i < GunParts.values().length; i++)
            addButton(new CategoryButton(GunParts.values()[i], i - 3));
        options = new HashMap<GunParts, List<Option>>() {{
            GunParts.getMinors().forEach((part) -> {
                put(part, new ArrayList<Option>() {{
                    AtomicInteger index = new AtomicInteger(0);
                    GunComponents.fromCategory(part).forEach((comp) -> {
                        add(new Option(index.getAndIncrement(), comp));
                    });
                }});
            });
        }};
        refreshOptions();
        if (minecraft != null)
            minecraft.keyboardListener.enableRepeatEvents(true);
        textField = new TextFieldWidget(font, guiLeft + 186, guiTop + 201, 66, 18, new StringTextComponent("Colour Hex"));
        textField.setCanLoseFocus(false);
        textField.setTextColor(-1);
        textField.setDisabledTextColour(-1);
        textField.setEnableBackgroundDrawing(false);
        textField.setMaxStringLength(35);
        textField.setResponder((colour) -> {
            colour = colour.replace("#", "");
            if (colour.length() == 6 && minecraft != null && minecraft.player != null)
            {
                try
                {
                    int hex = Integer.parseInt(colour, 16);
                    ItemStack stack = minecraft.player.getHeldItemMainhand();
                    if (stack.getItem() instanceof AssembleGunItem)
                        stack.getOrCreateTag().putInt("colour", hex);
                    textField.setText("");
                    ModPackets.INSTANCE.sendToServer(new ColourMessage(hex));
                }
                catch (Exception ignored)
                {

                }
            }
        });
        children.add(textField);
        setFocusedDefault(textField);
    }

    private double lastMouseXL = 0.0D;
    private double lastMouseYL = 0.0D;
    private boolean lastInFieldL = false;

    private double lastMouseXR = 0.0D;
    private double lastMouseYR = 0.0D;
    private boolean lastInFieldR = false;

    @Override
    public void mouseMoved(double mouseX, double mouseY)
    {
        if (minecraft == null)
            return;
        boolean inField = mouseInField(mouseX, mouseY) && AVAClientUtil.leftMouseDown(minecraft);
        if (inField)
        {
            if (lastInFieldL)
            {
                double x = mouseX - lastMouseXL;
                double y = mouseY - lastMouseYL;
                float sensitivity = 0.5F;
                rotation.add((float) -y / sensitivity, (float) x / sensitivity / 2.0F, 0.0F);
            }
            lastMouseXL = mouseX;
            lastMouseYL = mouseY;
        }
        lastInFieldL = inField;

        inField = mouseInField(mouseX, mouseY) && AVAClientUtil.rightMouseDown(minecraft);
        if (inField)
        {
            if (lastInFieldR)
            {
                double x = mouseX - lastMouseXR;
                double y = mouseY - lastMouseYR;
                float sensitivity = 1.0F;
                translation.add((float) x / sensitivity, (float) y / sensitivity, 0.0F);
            }
            lastMouseXR = mouseX;
            lastMouseYR = mouseY;
        }
        lastInFieldR = inField;
    }

    @Override
    public boolean mouseScrolled(double mouseX, double mouseY, double delta)
    {
        if (mouseInField(mouseX, mouseY))
            scale = (float) Math.max(0.0F, Math.min(40, scale + delta / 2.0F));
        return true;
    }

    private float scale = 25F;
    private final Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
    private final Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);

    private void renderGun(MatrixStack stack, ItemStack item)
    {
        int x = guiLeft - 80;
        int y = guiTop - 145;
        stack.push();

        MatrixStack matrixstack = new MatrixStack();
        matrixstack.scale(scale, scale, 0.1F);

        matrixstack.translate(10, 10, 100.0D);
        AVAClientUtil.rotateStack(matrixstack, new float[]{rotation.getX(), rotation.getY(), rotation.getZ()});
        matrixstack.translate(-10, -10, -100.0D);

        RenderSystem.pushMatrix();

        RenderSystem.translatef(x, y, 0.0F);
        RenderSystem.translatef(translation.getX(), translation.getY(), translation.getZ());
        AVAClientUtil.renderItemStack(matrixstack, item, 0, 0);


        stack.pop();
        RenderSystem.popMatrix();
    }

    @Override
    public void render(MatrixStack stack, int mouseX, int mouseY, float partialTicks)
    {
        if (minecraft == null || minecraft.player == null)
            return;
        PlayerEntity player = minecraft.player;
        ItemStack item = player.getHeldItemMainhand();
        stack.push();
        stack.translate(0.0F, 0.0F, -100.0F);
        renderBackground(stack);
        super.render(stack, mouseX, mouseY, partialTicks);



        stack.translate(guiLeft, guiTop, 0.0F);
        AbstractGui.fill(stack, 19, 19, 237, 115, colour);

        stack.pop();
        renderGun(stack, item);

        buttons.forEach((button) -> {
            if (button instanceof Option || button instanceof CategoryButton)
                button.renderToolTip(stack, mouseX, mouseY);
        });

        textField.render(stack, mouseX, mouseY, partialTicks);
    }

    @Override
    public void renderBackground(MatrixStack stack)
    {
        if (minecraft == null)
            return;
        super.renderBackground(stack);
        RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
        minecraft.getTextureManager().bindTexture(TEXTURE);
        int x = (width - TEXTURE_WINDOW_WIDTH) / 2;
        int y = (height - TEXTURE_WINDOW_HEIGHT) / 2;
        blit(stack, x, y, 0, 0, TEXTURE_WINDOW_WIDTH, TEXTURE_WINDOW_HEIGHT);
    }

    private boolean mouseInField(double x, double y)
    {
        return x > guiLeft + 19 && x <= guiLeft + 237 && y > guiTop + 19 && y <= guiTop + 114;
    }

    private Map<GunParts, List<Option>> options;
    private List<Option> listedOptions = new ArrayList<>();
    public void refreshOptions()
    {
        if (minecraft == null || minecraft.player == null)
            return;
        buttons.removeAll(listedOptions);
        children.removeAll(listedOptions);
        listedOptions = new ArrayList<>(options.get(selectedCategory));
        GunComponentsManager manager = GunComponentsManager.getCap(minecraft.player.getHeldItemMainhand());
        for (Option option : listedOptions)
        {
            GunComponents component = option.component;
            if (manager.isInstalled(component))
            {
                option.phase = Phase.VALID;
                if (component.getCategory().mustInstall())
                    option.tip = "You can not uninstall essential parts";
                else option.tip = "Uninstall";
            }
            else
            {
                StringBuilder tip = new StringBuilder("Require ");
                for (GunParts req : component.getCategory().getRequirements())
                    if (!manager.isPartInstalled(req))
                    {
                        option.phase = Phase.INVALID;
                        tip.append(AVACommonUtil.toDisplayString(req.name())).append(",");
                    }
                for (GunComponents req : component.getRequirements())
                    if (!manager.isInstalled(req))
                    {
                        option.phase = Phase.INVALID;
                        tip.append(AVACommonUtil.toDisplayString(req.name())).append(",");
                    }
                tip.deleteCharAt(tip.length() - 1);
                if (tip.length() > 7)
                    option.tip = tip.toString();
                else
                {
                    option.phase = Phase.EMPTY;
                    option.tip = "Install";
                }
            }
        }
        buttons.addAll(listedOptions);
        children.addAll(listedOptions);
    }

    @Override
    public boolean isPauseScreen()
    {
        return false;
    }

    class CategoryButton extends Button
    {
        private final GunParts category;
        private boolean selected;
        public CategoryButton(GunParts category, int index)
        {
            super(ModificationScreen.this.guiLeft + 134 + index % 5 * 24, ModificationScreen.this.guiTop + 122 + index / 5 * 25, 18, 18, new StringTextComponent(""), (button) -> {});
            this.category = category;
            if (index == 0)
                selected = true;
        }

        @Override
        public void renderToolTip(MatrixStack matrixStack, int mouseX, int mouseY)
        {
            if (isHovered())
                ModificationScreen.this.renderTooltip(matrixStack, new StringTextComponent(AVACommonUtil.toDisplayString(category.name())), mouseX, mouseY);
        }

        @Override
        public void renderWidget(MatrixStack stack, int mouseX, int mouseY, float partialTicks)
        {
            Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
            RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            int tx = 80;
            if (selected)
                tx += 36;
            else if (isHovered())
                tx += 18;
            if (tx != 80)
                blit(stack, x, y, tx, 224, width, height);
        }

        @Override
        public void onClick(double mouseX, double mouseY)
        {
            ModificationScreen.this.selectedCategory = category;
            ModificationScreen.this.refreshOptions();
            children.stream().filter((but) -> but instanceof CategoryButton).forEach((but) -> ((CategoryButton) but).selected = false);
            selected = true;
        }
    }

    class Option extends PhaseButton
    {
        private final int index;
        private final GunComponents component;
        private String tip = "";

        public Option(int index, GunComponents component)
        {
            this(index, (button) -> {}, EMPTY_TOOLTIP, component);
        }

        public Option(int index, IPressable pressedAction, GunComponents component)
        {
            this(index, pressedAction, EMPTY_TOOLTIP, component);
        }

        public Option(int index, IPressable pressedAction, ITooltip onTooltip, GunComponents component)
        {
            this(index, ModificationScreen.this.guiLeft + 4 + index / 3 * 63, ModificationScreen.this.guiTop + 117 + index % 3 * 35, 15, 15, new StringTextComponent(""), pressedAction, onTooltip, component);
        }

        public Option(int index, int x, int y, int width, int height, ITextComponent title, IPressable pressedAction, ITooltip onTooltip, GunComponents component)
        {
            super(x, y, width, height, title, pressedAction, onTooltip);
            this.index = index;
            this.component = component;
        }

        @Override
        public void renderWidget(MatrixStack stack, int mouseX, int mouseY, float partialTicks)
        {
            Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
            RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            int tx = 0;
            if (phase != Phase.INVALID && isHovered())
                tx += width;
            if (phase == Phase.VALID)
                tx += width * 2;
            else if (phase == Phase.INVALID)
                tx += width * 4;
            blit(stack, x, y, tx, 229, width, height);

            ItemStack item = new ItemStack(Items.IRON_INGOT, component.getCost());
            itemRenderer.renderItemIntoGUI(item, x + 29, y - 1);
            itemRenderer.renderItemOverlayIntoGUI(font, item, x + 29, y - 1, null);

            RenderSystem.pushMatrix();
            float scale = 0.8F;
            RenderSystem.scalef(scale, scale, scale);
            StringBuilder text = new StringBuilder(AVACommonUtil.toDisplayString(component.name()));
            int width = font.getStringWidth(text.toString());
            if (width > 70)
            {
                String[] split = text.toString().split(" ");
                drawCenteredString(stack, font, split[0] + " " + split[1], (int) ((x + 30) / scale), (int) ((y + 17) / scale), AVAConstants.AVA_HUD_TEXT_ORANGE);
                text = new StringBuilder();
                for (int i = 2; i < split.length; i++)
                {
                    text.append(split[i]);
                    if (i < split.length - 1)
                        text.append(" ");
                }
                drawCenteredString(stack, font, text.toString(), (int) ((x + 30) / scale), (int) ((y + 24) / scale), AVAConstants.AVA_HUD_TEXT_ORANGE);
            }
            else drawCenteredString(stack, font, text.toString(), (int) ((x + 30) / scale), (int) ((y + 20) / scale), AVAConstants.AVA_HUD_TEXT_ORANGE);
            RenderSystem.popMatrix();
        }

        @Override
        public void renderToolTip(MatrixStack stack, int mouseX, int mouseY)
        {
            if (!tip.isEmpty() && isHovered())
                renderTooltip(stack, new StringTextComponent(tip), mouseX, mouseY);
            if (mouseX > x && mouseX <= x + 59 && mouseY > y + height && mouseY <= y + 32)
            {
                List<ITextComponent> texts = new ArrayList<>();
                for (Map.Entry<GunComponents.ComponentAttributes, Float> entry : component.getAttributesMap().entrySet())
                    if (!entry.getKey().isDefault(entry.getValue()))
                    {
                        GunComponents.ComponentAttributes attribute = entry.getKey();
                        float value = entry.getValue();
                        texts.add(new StringTextComponent(AVACommonUtil.toDisplayString(attribute.name()) + ": ").appendSibling(new StringTextComponent(String.valueOf(value)).setStyle(Style.EMPTY.applyFormatting(attribute.isBeneficial(value) ? TextFormatting.GREEN : TextFormatting.RED))));
                    }
                func_243308_b(stack, texts, mouseX, mouseY);
            }
        }

        @Override
        public void onClick(double mouseX, double mouseY)
        {
            if (phase != Phase.INVALID && minecraft != null && minecraft.player != null)
            {
                if (component.getCategory().mustInstall() && phase == Phase.VALID)
                    return;
                int type = phase == Phase.EMPTY ? ModifyMessage.INSTALL : ModifyMessage.UNINSTALL;
                if (ModifyMessage.performModification(minecraft.player, minecraft.player.getHeldItemMainhand(), component, type))
                    ModPackets.INSTANCE.sendToServer(new ModifyMessage(component, type));
            }
        }
    }

    static class PhaseButton extends Button
    {
        protected Phase phase = Phase.EMPTY;
        public PhaseButton(int x, int y, int width, int height, ITextComponent title, IPressable pressedAction)
        {
            super(x, y, width, height, title, pressedAction);
        }

        public PhaseButton(int x, int y, int width, int height, ITextComponent title, IPressable pressedAction, ITooltip onTooltip)
        {
            super(x, y, width, height, title, pressedAction, onTooltip);
        }

        public PhaseButton setPhase(Phase phase)
        {
            this.phase = phase;
            return this;
        }
    }

    class BackgroundColourButton extends Button
    {
        private final int index;
        public BackgroundColourButton(int x, int y, int index)
        {
            super(x, y, 5, 5, new StringTextComponent(""), (button) -> {});
            this.index = index;
        }

        @Override
        public void onPress()
        {
            ModificationScreen.this.colour = BackgroundColours.fromIndex(index).colour;
        }

        @Override
        public void renderWidget(MatrixStack stack, int mouseX, int mouseY, float partialTicks)
        {
            Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
            RenderSystem.color4f(1.0F, 1.0F, 1.0F, 1.0F);
            blit(stack, x, y, 5 * index, 224, width, height);
        }
    }

    enum Phase
    {
        EMPTY,
        VALID,
        INVALID;

        public boolean isEmpty()
        {
            return this == EMPTY;
        }
    }

    enum BackgroundColours
    {
        BLACK(0, 0xFF000000),
        WHITE(1, 0xFFFFFFFF),
        RED(2, 0xFFFF0000),
        YELLOW(3, 0xFFF4FF00),
        BLUE(4, 0xFF00DEFF),
        GREY(5, 0xFFD1D1D1),
        PURPLE(6, 0xFFA2A6CF),
        GREEN(7, 0xFFBFCFA2),
        ORANGE(8, 0xFFCFBCA2),
        LIGHT_BLUE(9, 0xFFA2C9CF);
        private final int index;
        public final int colour;
        BackgroundColours(int index, int colour)
        {
            this.index = index;
            this.colour = colour;
        }

        public static BackgroundColours fromIndex(int index)
        {
            return Arrays.stream(values()).filter((colour) -> colour.index == index).findFirst().orElse(BLACK);
        }
    }
}
