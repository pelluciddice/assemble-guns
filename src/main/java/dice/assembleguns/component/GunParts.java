package dice.assembleguns.component;

import dice.assembleguns.component.components.GunComponents;
import dice.assembleguns.component.components.parts.GunPart;
import net.minecraft.util.ResourceLocation;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Stream;

public enum GunParts
{
    @Deprecated
    FRONT(null) {
    },
    BODY(null){
    },
    BACK(null){
    },

    HANDGUARD(FRONT){
    },
    BARREL(HANDGUARD){
    },
    FRONT_SIGHT(HANDGUARD){
        @Override
        GunParts[] require()
        {
            return new GunParts[] {HANDGUARD};
        }
    },
    MUZZLE(HANDGUARD){
        @Override
        GunParts[] require()
        {
            return new GunParts[] {BARREL};
        }
    },
    RAIL_LEFT(HANDGUARD){
        @Override
        GunParts[] require()
        {
            return new GunParts[] {HANDGUARD};
        }

    },
    RAIL_RIGHT(HANDGUARD){
        @Override
        GunParts[] require()
        {
            return new GunParts[] {HANDGUARD};
        }
    },
    RAIL_BOTTOM(HANDGUARD){
        @Override
        GunParts[] require()
        {
            return new GunParts[] {HANDGUARD};
        }
    },
    RAIL_BOTTOM_GRIP(HANDGUARD){
        @Override
        GunParts[] require()
        {
            return new GunParts[] {HANDGUARD};
        }
    },

    CHAMBER(BODY){

        @Override
        public GunComponents getDefault()
        {
            return GunComponents.CHAMBER_AUTOMATIC;
        }
    },
    GRIP(BODY){

        @Override
        public GunComponents getDefault()
        {
            return GunComponents.GRIP_STEEL;
        }
    },
    MAGAZINE_WELL(BODY){

        @Override
        public GunComponents getDefault()
        {
            return GunComponents.MAGAZINE_WELL_SMALL;
        }
    },
    MAGAZINE(BODY){

        @Override
        public GunComponents getDefault()
        {
            return GunComponents.MAGAZINE_25_CAL;
        }
    },
    SCOPE(BODY){
    },
    TRIGGER(BODY){

        @Override
        public GunComponents getDefault()
        {
            return GunComponents.TRIGGER_MEDIUM_PRESSURED;
        }
    },

    STOCK(BACK){
    },
    STOCK_TOP(BACK){
        @Override
        GunParts[] require()
        {
            return new GunParts[] {STOCK};
        }
    },
    BUTT(BACK){
        @Override
        GunParts[] require()
        {
            return new GunParts[] {STOCK};
        }
    },;

    @Nullable
    private final GunParts major;

    GunParts(@Nullable GunParts major)
    {
        this.major = major;
    }

    public String getName()
    {
        return super.name().toLowerCase(Locale.ROOT);
    }

    public boolean mustInstall()
    {
        return hasDefault();
    }

    public boolean hasDefault()
    {
        return getDefault() != null;
    }

    public GunComponents getDefault()
    {
        return null;
    }

    public List<GunParts> getRequirements()
    {
        List<GunParts> parts = new ArrayList<>();
        for (GunParts part : require())
        {
            parts.add(part);
            parts.addAll(part.getRequirements());
        }
        return parts;
    }

    GunParts[] require()
    {
        return new GunParts[] {};
    }

    public boolean isMajor()
    {
        return !isMinor();
    }

    public boolean isMinor()
    {
        return getMajor() != null;
    }

    @Nullable
    public GunParts getMajor()
    {
        return major;
    }

    public static Stream<GunParts> getMinors()
    {
        return Arrays.stream(values()).filter(GunParts::isMinor);
    }

    public static Stream<GunParts> getFromMajors(GunParts major)
    {
        return Arrays.stream(values()).filter((part) -> part.major == major);
    }

    @Override
    public String toString()
    {
        return "GunParts{" +
                name() +
                ", major=" + major +
                '}';
    }
}
