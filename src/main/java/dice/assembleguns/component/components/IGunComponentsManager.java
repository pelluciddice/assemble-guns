package dice.assembleguns.component.components;

import dice.assembleguns.component.GunParts;
import dice.assembleguns.component.components.parts.GunPart;

import java.util.HashMap;
import java.util.List;

public interface IGunComponentsManager
{
    List<GunPart> getParts();

    HashMap<String, GunPart> getKeys();

    boolean isInstalled(GunComponents component);

    boolean isPartInstalled(GunParts part);

    void install(GunComponents component);

    void unInstall(GunComponents component);
}
