package dice.assembleguns.component.components;

import dice.assembleguns.AssembleGuns;
import dice.assembleguns.component.GunParts;
import dice.assembleguns.items.AssembleGunItem;
import dice.assembleguns.items.GunComponentItem;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IItemProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.registries.ForgeRegistries;
import pellucid.ava.items.guns.AVAItemGun;

import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Stream;

import static dice.assembleguns.component.GunParts.*;
import static dice.assembleguns.component.components.GunComponents.ComponentAttributes.*;
import static dice.assembleguns.component.components.GunComponents.Builder.*;

public enum GunComponents implements IItemProvider
{
    FLASH_HIDER(weightAndCostWithDurability(0.0085F, 10, 100).put(ATTACK_DAMAGE, 7.5F), MUZZLE),
    MUZZLE_BRAKE(weightAndCostWithDurability(0.005F, 15, 100).put(STABILITY, 0.45F).put(VOLUME, 0.35F), MUZZLE),
    SUPPRESSOR(weightAndCostWithDurability(0.0125F, 20, 80).put(ATTACK_DAMAGE, -6.0F).put(RANGE, -10.0F).put(VOLUME, -0.8F), MUZZLE),

    FRONT_SIGHT_SMALL(weightAndCostWithDurability(0.005F, 10, 100).put(ACCURACY, 0.1F).put(MAGNIFICATION, 0.1F), FRONT_SIGHT),
    FRONT_SIGHT_MEDIUM(weightAndCostWithDurability(0.0065F, 15, 110).put(ACCURACY, 0.15F).put(MAGNIFICATION, 0.175F), FRONT_SIGHT),
    FRONT_SIGHT_LARGE(weightAndCostWithDurability(0.008F, 20, 120).put(ACCURACY, 0.2F).put(MAGNIFICATION, 0.25F), FRONT_SIGHT),

    SHORT_BARREL_STEEL(weightAndCostWithDurability(0.05F, 60, 300).put(RANGE, 18.0F).put(ACCURACY, 0.25F), BARREL),
    SHORT_BARREL_ALUMINUM(weightAndCostWithDurability(0.0465F, 30, 250).put(RANGE, 13.0F).put(ACCURACY, 0.225F), BARREL),
    SHORT_BARREL_CARBON_FIBRE(weightAndCostWithDurability(0.0425F, 80, 350).put(RANGE, 15.5F).put(ACCURACY, 0.2F), BARREL),

    LONG_BARREL_STEEL(weightAndCostWithDurability(0.0675F, 120, 350).put(RANGE, 36.0F).put(ACCURACY, 0.6F), BARREL),
    LONG_BARREL_ALUMINUM(weightAndCostWithDurability(0.059F, 60, 300).put(RANGE, 26.0F).put(ACCURACY, 0.55F), BARREL),
    LONG_BARREL_CARBON_FIBRE(weightAndCostWithDurability(0.055F, 160, 400).put(RANGE, 31.0F).put(ACCURACY, 0.5F), BARREL),

    SHORT_FRONT_STEEL(weightAndCostWithDurability(0.0625F, 80, 300).put(STABILITY, 0.75F), HANDGUARD),
    SHORT_FRONT_WOOD(weightAndCostWithDurability(0.0615F, 65, 250).put(STABILITY, 0.65F), HANDGUARD),
    SHORT_FRONT_CARBON_FIBRE(weightAndCostWithDurability(0.059F, 100, 350).put(STABILITY, 0.55F), HANDGUARD),

    LONG_FRONT_STEEL(weightAndCostWithDurability(0.0725F, 140, 400).put(STABILITY, 1.15F), HANDGUARD),
    LONG_FRONT_WOOD(weightAndCostWithDurability(0.0715F, 120, 350).put(STABILITY, 1.05F), HANDGUARD),
    LONG_FRONT_CARBON_FIBRE(weightAndCostWithDurability(0.07F, 160, 450).put(STABILITY, 0.95F), HANDGUARD),

    RED_LASER_LEFT(weightAndCostWithDurability(0.0075F, 15, 50).put(ACCURACY, 0.05F).notRecolourable(), RAIL_LEFT),
    GREEN_LASER_LEFT(weightAndCostWithDurability(0.0085F, 20, 50).put(ACCURACY, 0.1F).notRecolourable(), RAIL_LEFT),
    BLUE_LASER_LEFT(weightAndCostWithDurability(0.0095F, 25, 50).put(ACCURACY, 0.15F).notRecolourable(), RAIL_LEFT),

    RED_LASER_RIGHT(weightAndCostWithDurability(0.0075F, 15, 50).put(ACCURACY, 0.05F), RAIL_RIGHT),
    GREEN_LASER_RIGHT(weightAndCostWithDurability(0.0085F, 20, 50).put(ACCURACY, 0.1F), RAIL_RIGHT),
    BLUE_LASER_RIGHT(weightAndCostWithDurability(0.0095F, 25, 50).put(ACCURACY, 0.15F), RAIL_RIGHT),

    RED_LASER_BOTTOM(weightAndCostWithDurability(0.0075F, 15, 50).put(ACCURACY, 0.05F), RAIL_BOTTOM),
    GREEN_LASER_BOTTOM(weightAndCostWithDurability(0.0085F, 20, 50).put(ACCURACY, 0.1F), RAIL_BOTTOM),
    BLUE_LASER_BOTTOM(weightAndCostWithDurability(0.0095F, 25, 50).put(ACCURACY, 0.15F), RAIL_BOTTOM),

    HANDGUARD_WOOD(weightAndCostWithDurability(0.035F, 60, 400).put(STABILITY, 0.65F), RAIL_BOTTOM_GRIP),
    HANDGUARD_CARBON_FIBRE(weightAndCostWithDurability(0.0295F, 70, 450).put(STABILITY, 0.6F), RAIL_BOTTOM_GRIP),
    VERTICAL_GRIP(weightAndCostWithDurability(0.025F, 55, 350).put(STABILITY, 0.425F), RAIL_BOTTOM_GRIP),
    ANGLED_GRIP(weightAndCostWithDurability(0.0275F, 65, 350).put(STABILITY, 0.55F), RAIL_BOTTOM_GRIP),



    CHAMBER_AUTOMATIC(weightAndCostWithDurability(0.07F, 120, 1000, true).put(FIRE_RATE, 2.0F).put(STABILITY, 0.1F).put(ACCURACY, -0.1F).put(PITCH, 0.3F).put(ATTACK_DAMAGE, 17.0F), CHAMBER),
    CHAMBER_SEMI_AUTOMATIC(weightAndCostWithDurability(0.07F, 120, 900).put(FIRE_RATE, 10.0F).put(STABILITY, -6F).put(ACCURACY, 0.1F).put(PITCH, 0.05F).put(CAPACITY, -5).put(ATTACK_DAMAGE, 42.0F), CHAMBER),
    CHAMBER_MANUAL(weightAndCostWithDurability(0.07F, 120, 800).put(FIRE_RATE, 20.0F).put(STABILITY, -10F).put(ACCURACY, 0.25F).put(PITCH, -0.3F).put(CAPACITY, -10).put(ATTACK_DAMAGE, 56.0F), CHAMBER),

    GRIP_STEEL(weightAndCostWithDurability(0.0275F, 45, 400, true).put(STABILITY, 0.35F), GRIP),
    GRIP_WOOD(weightAndCostWithDurability(0.026F, 50, 350).put(STABILITY, 0.35F), GRIP),
    GRIP_CARBON_FIBRE(weightAndCostWithDurability(0.0245F, 55, 450).put(STABILITY, 0.315F), GRIP),

    MAGAZINE_WELL_SMALL(weightAndCostWithDurability(0.03F, 50, 200).put(CAPACITY, -5).put(INITIAL_ACCURACY, 0.65F), MAGAZINE_WELL),
    MAGAZINE_WELL_MEDIUM(weightAndCostWithDurability(0.0335F, 60, 300).put(CAPACITY, 0).put(INITIAL_ACCURACY, 0.25F), MAGAZINE_WELL),
    MAGAZINE_WELL_LARGE(weightAndCostWithDurability(0.038F, 70, 400).put(CAPACITY, 5).put(INITIAL_ACCURACY, 0.15F), MAGAZINE_WELL),

    MAGAZINE_SHOTGUN(weightAndCostWithDurability(0.0225F, 30, 200).put(PITCH, 0.15F).put(INITIAL_ACCURACY, -1.3F).put(STABILITY, -2.0F).put(ATTACK_DAMAGE, -30.0F).put(CAPACITY, 15).put(ACCURACY, -1.3F).put(RANGE, -20).put(BULLETS_PER_SHOT, 8), MAGAZINE) {
        @Override
        GunComponents[] require()
        {
            return new GunComponents[] {CHAMBER_SEMI_AUTOMATIC};
        }
    },
    MAGAZINE_25_CAL(weightAndCostWithDurability(0.025F, 35, 200, true).put(PITCH, 0.3F).put(INITIAL_ACCURACY, 0.1F).put(STABILITY, -0.25F).put(ATTACK_DAMAGE, 4.0F).put(CAPACITY, 40).put(ACCURACY, 0.15F), MAGAZINE),
    MAGAZINE_40_CAL(weightAndCostWithDurability(0.0275F, 40, 200).put(PITCH, -0.15F).put(INITIAL_ACCURACY, 0.35F).put(STABILITY, -0.425F).put(ATTACK_DAMAGE, 6.0F).put(CAPACITY, 30).put(ACCURACY, 0.3F), MAGAZINE),
    MAGAZINE_50_CAL(weightAndCostWithDurability(0.03F, 45, 200).put(PITCH, -0.3F).put(INITIAL_ACCURACY, 0.75F).put(STABILITY, -2.2F).put(ATTACK_DAMAGE, 10.0F).put(CAPACITY, 20).put(ACCURACY, 0.45F), MAGAZINE),

    SCOPE_IRON_SIGHT(weightAndCostWithDurability(0.0175F, 10, 500).put(ACCURACY, 0.2F).put(MAGNIFICATION, 0.1F), SCOPE),
    SCOPE_RED_DOT(weightAndCostWithDurability(0.019F, 15, 450).put(ACCURACY, 0.3F).put(MAGNIFICATION, 0.2F), SCOPE),
    SCOPE_SMALL(weightAndCostWithDurability(0.021F, 20, 400).put(ACCURACY, 0.4F).put(MAGNIFICATION, 0.3F), SCOPE),
    SCOPE_MEDIUM(weightAndCostWithDurability(0.023F, 30, 350).put(ACCURACY, 0.5F).put(MAGNIFICATION, 0.5F), SCOPE),
    SCOPE_LARGE(weightAndCostWithDurability(0.0265F, 45, 300).put(ACCURACY, 0.6F).put(MAGNIFICATION, 0.7F), SCOPE),

    TRIGGER_SOFT_PRESSURED(weightAndCostWithDurability(0.002F, 5, 200).put(PITCH, 0.2F).put(INITIAL_ACCURACY, 0.8F).put(STABILITY, -0.25F).put(INITIAL_ACCURACY, 0.3F).put(ATTACK_DAMAGE, 3.0F).put(RANGE, 8), TRIGGER),
    TRIGGER_MEDIUM_PRESSURED(weightAndCostWithDurability(0.005F, 10, 300, true).put(PITCH, 0.05F).put(INITIAL_ACCURACY, 1.4F).put(INITIAL_ACCURACY, 0.5F).put(STABILITY, -0.075F).put(ATTACK_DAMAGE, 5.0F).put(RANGE, 13), TRIGGER),
    TRIGGER_HEAVY_PRESSURED(weightAndCostWithDurability(0.0085F, 20, 400).put(PITCH, -0.2F).put(INITIAL_ACCURACY, 2.0F).put(ACCURACY, 1.0F).put(STABILITY, 0.225F).put(ATTACK_DAMAGE, 10.0F).put(RANGE, 18), TRIGGER),



    STOCK_STEEL(weightAndCostWithDurability(0.075F, 60, 400).put(STABILITY, 0.8F), STOCK),
    STOCK_WOOD(weightAndCostWithDurability(0.07F, 50, 350).put(STABILITY, 0.7F), STOCK),
    STOCK_CARBON_FIBRE(weightAndCostWithDurability(0.0575F, 80, 450).put(STABILITY, 0.5F), STOCK),

    STOCK_TOP_STEEL(weightAndCostWithDurability(0.02F, 30, 100).put(STABILITY, 0.25F), STOCK_TOP),
    STOCK_TOP_WOOD(weightAndCostWithDurability(0.0175F, 20, 50).put(STABILITY, 0.2F), STOCK_TOP),
    STOCK_TOP_CARBON_FIBRE(weightAndCostWithDurability(0.01F, 40, 150).put(STABILITY, 0.165F), STOCK_TOP),

    BUTT_STEEL(weightAndCostWithDurability(0.0075F, 20, 100).put(STABILITY, 0.2F).put(ACCURACY, 0.15F), BUTT),
    BUTT_WOOD(weightAndCostWithDurability(0.005F, 15, 50).put(STABILITY, 0.1F).put(ACCURACY, 0.25F), BUTT),
    BUTT_SILICON(weightAndCostWithDurability(0.002F, 10, 75).put(STABILITY, 0.15F).put(ACCURACY, 0.175F), BUTT),
    ;
    
    private final HashMap<ComponentAttributes, Float> map;
    private final int cost;
    private final GunParts category;
    private final boolean isDefaultInstalled;
    private final boolean recolourable;

    GunComponents(Builder builder, GunParts minorParts)
    {
        this.map = builder.map;
        this.cost = builder.cost;
        this.isDefaultInstalled = builder.isDefaultInstalled;
        this.category = minorParts;
        this.recolourable = builder.recolourable;
    }

    public List<GunComponents> getRequirements()
    {
        List<GunComponents> parts = new ArrayList<>();
        for (GunComponents part : require())
        {
            parts.add(part);
            parts.addAll(part.getRequirements());
        }
        return parts;
    }

    GunComponents[] require()
    {
        return new GunComponents[] {};
    }

    public ItemStack asItemStack()
    {
        return new ItemStack(asItem());
    }

    @Override
    public Item asItem()
    {
        return ForgeRegistries.ITEMS.getValue(new ResourceLocation(AssembleGuns.MODID, name().toLowerCase(Locale.ROOT)));
    }

    public HashMap<ComponentAttributes, Float> getAttributesMap()
    {
        return map;
    }

    public int getCost()
    {
        return cost;
    }

    public GunParts getCategory()
    {
        return category;
    }

    public boolean isDefaultInstalled()
    {
        return isDefaultInstalled;
    }

    public boolean isRecolourable()
    {
        return recolourable;
    }

    public static Stream<GunComponents> fromCategory(GunParts category)
    {
        return Arrays.stream(values()).filter((comp) -> comp.category == category);
    }

    public static boolean isLongFront(GunComponents component)
    {
        return component == LONG_FRONT_STEEL || component == LONG_FRONT_WOOD || component == LONG_FRONT_CARBON_FIBRE;
    }

    public static boolean isLongBarrel(GunComponents component)
    {
        return component == LONG_BARREL_STEEL || component == LONG_BARREL_ALUMINUM || component == LONG_BARREL_CARBON_FIBRE;
    }

    static class Builder
    {
        private final HashMap<ComponentAttributes, Float> map = ComponentAttributes.defaultMap();
        private final int cost;
        private final boolean isDefaultInstalled;
        private String model;
        private boolean recolourable = true;

        private Builder(float weight, int cost, int durability)
        {
            this(weight, cost, durability, false);
        }

        private Builder(float weight, int cost, int durability, boolean isDefaultInstalled)
        {
            map.put(WEIGHT, weight);
            map.put(DURABILITY, (float) durability);
            this.cost = cost;
            this.isDefaultInstalled = isDefaultInstalled;
        }

        public static Builder weightAndCostWithDurability(float weight, int cost, int durability)
        {
            return weightAndCostWithDurability(weight, cost, durability, false);
        }

        public static Builder weightAndCostWithDurability(float weight, int cost, int durability, boolean isDefaultInstalled)
        {
            return new Builder(weight, cost, durability, isDefaultInstalled);
        }

        public Builder put(ComponentAttributes attribute, float amount)
        {
            map.put(attribute, amount);
            return this;
        }

        public Builder notRecolourable()
        {
            recolourable = false;
            return this;
        }
    }

    public enum ComponentAttributes
    {
        ATTACK_DAMAGE(true),
        STABILITY(true),
        INITIAL_ACCURACY(true),
        ACCURACY(true),
        RANGE(true),
        FIRE_RATE(true),
        VOLUME(false),
        PITCH(true, true),
        WEIGHT(false),
        DURABILITY(true),
        CAPACITY(true),
        RELOAD_TIME(false),
        MAGNIFICATION(true),
        BULLETS_PER_SHOT(true);

        private final boolean positiveBeneficial;
        private final float defaultValue;
        private final boolean hidden;

        ComponentAttributes(boolean positiveBeneficial)
        {
            this(positiveBeneficial, false);
        }

        ComponentAttributes(boolean positiveBeneficial, boolean hidden)
        {
            this(positiveBeneficial, 0.0F, hidden);
        }

        ComponentAttributes(boolean positiveBeneficial, float defaultValue, boolean hidden)
        {
            this.positiveBeneficial = positiveBeneficial;
            this.defaultValue = defaultValue;
            this.hidden = hidden;
        }

        public boolean isHidden()
        {
            return hidden;
        }

        public boolean isBeneficial(float x)
        {
            return x > 0.0F == positiveBeneficial;
        }

        public boolean isDefault(float x)
        {
            return x == defaultValue;
        }

        public static HashMap<ComponentAttributes, Float> defaultMap()
        {
            return new HashMap<ComponentAttributes, Float>() {{
                for (ComponentAttributes value : ComponentAttributes.values())
                    put(value, value.defaultValue);
            }};
        }

        public static Stream<ComponentAttributes> getVisibleAttributes()
        {
            return Arrays.stream(ComponentAttributes.values()).filter((attribute) -> !attribute.isHidden());
        }
    }

    public enum AttachingPoints
    {
        MUZZLE_BARREL(BARREL, MUZZLE),
        BARREL_FRONT(HANDGUARD, BARREL),
        FRONT_FRONT_SIGHT(HANDGUARD, FRONT_SIGHT),
        FRONT_RAIL_LEFT(HANDGUARD, RAIL_LEFT),
        FRONT_RAIL_RIGHT(HANDGUARD, RAIL_RIGHT),
        FRONT_RAIL_BOTTOM(HANDGUARD, RAIL_BOTTOM),
        FRONT_RAIL_BOTTOM_GRIP(HANDGUARD, RAIL_BOTTOM_GRIP),

        BODY_MAGAZINE_WELL(BODY, MAGAZINE_WELL),
        BODY_MAGAZINE(BODY, MAGAZINE),
        BODY_TRIGGER(BODY, TRIGGER),
        BODY_GRIP(BODY, GRIP),
        BODY_CHAMBER(BODY, CHAMBER),
        BODY_SCOPE(BODY, SCOPE),

        STOCK_STOCK_TOP(STOCK, STOCK_TOP),
        STOCK_BUTT(STOCK, BUTT)
        ;

        private final GunParts[] connected;

        AttachingPoints(GunParts... connected)
        {
            this.connected = connected;
        }

        public static AttachingPoints getFromParts(GunParts... connected)
        {
            return Arrays.stream(values()).filter((point) -> {
                for (GunParts part : point.connected)
                    if (Arrays.stream(connected).noneMatch((p) -> p == part))
                        return false;
                return true;
            }).findFirst().orElse(MUZZLE_BARREL);
        }

        public static AttachingPoints getFromSecondPart(GunParts connected)
        {
            return Arrays.stream(values()).filter((point) -> point.connected[1] == connected).findFirst().orElse(MUZZLE_BARREL);
        }

        public GunParts[] getConnected()
        {
            return connected;
        }
    }

    public enum GunComponentModels
    {
        FLASH_HIDER_MODEL(of(FLASH_HIDER).put(AttachingPoints.MUZZLE_BARREL, new Vector3d(8.0D, 7.6484D, -1.6053D))),
        MUZZLE_BRAKE_MODEL(of(MUZZLE_BRAKE).put(AttachingPoints.MUZZLE_BARREL, new Vector3d(8.0D, 7.6513D, -1.6053D))),
        SUPPRESSOR_MODEL(of(SUPPRESSOR).put(AttachingPoints.MUZZLE_BARREL, new Vector3d(8.0D, 7.6513D, -1.6053D))),

        FRONT_SIGHT_SMALL_MODEL(of(FRONT_SIGHT_SMALL).put(AttachingPoints.FRONT_FRONT_SIGHT, new Vector3d(8D, 8.5135D, 4.5222D))),
        FRONT_SIGHT_MEDIUM_MODEL(of(FRONT_SIGHT_MEDIUM).put(AttachingPoints.FRONT_FRONT_SIGHT, new Vector3d(8D, 8.5135D, 4.5222D))),
        FRONT_SIGHT_LARGE_MODEL(of(FRONT_SIGHT_LARGE).put(AttachingPoints.FRONT_FRONT_SIGHT, new Vector3d(8D, 8.5135D, 4.5222D))),

        SHORT_BARREL_MODEL(of("short_barrel", SHORT_BARREL_STEEL, SHORT_BARREL_ALUMINUM, SHORT_BARREL_CARBON_FIBRE).fixed().put(AttachingPoints.MUZZLE_BARREL, new Vector3d(8.0D, 7.6513D, 2.9846D))),

        LONG_BARREL_MODEL(of("long_barrel", LONG_BARREL_STEEL, LONG_BARREL_ALUMINUM, LONG_BARREL_CARBON_FIBRE).fixed().put(AttachingPoints.MUZZLE_BARREL, new Vector3d(8.0D, 7.6513D, -1.6053D))),

        SHORT_FRONT_STEEL_MODEL(of(SHORT_FRONT_STEEL).fixed()
                .put(AttachingPoints.FRONT_FRONT_SIGHT, new Vector3d(8D, 8.4851D, 3.8003D))
                .put(AttachingPoints.FRONT_RAIL_LEFT, new Vector3d(7.375D, 7.6943D, 3.8596D))
                .put(AttachingPoints.FRONT_RAIL_RIGHT, new Vector3d(8.625D, 7.6943D, 3.8596D))
                .put(AttachingPoints.FRONT_RAIL_BOTTOM, new Vector3d(8D, 6.9404D, 3.8596D))
                .put(AttachingPoints.FRONT_RAIL_BOTTOM_GRIP, new Vector3d(8.0D, 6.9404D, 6.8127D))),
        SHORT_FRONT_WOOD_MODEL(of(SHORT_FRONT_WOOD).fixed()
                .put(AttachingPoints.FRONT_FRONT_SIGHT, new Vector3d(8D, 8.5135D, 4.5222D))
                .put(AttachingPoints.FRONT_RAIL_LEFT, new Vector3d(7.3855D, 7.7298D, 4.5431))
                .put(AttachingPoints.FRONT_RAIL_RIGHT, new Vector3d(8.6145D, 7.7298D, 4.5431D))
                .put(AttachingPoints.FRONT_RAIL_BOTTOM, new Vector3d(8D, 6.9446D, 4.5431D))
                .put(AttachingPoints.FRONT_RAIL_BOTTOM_GRIP, new Vector3d(8.0D, 6.9404D, 7.0751D))),
        SHORT_FRONT_CARBON_FIBRE_MODEL(of(SHORT_FRONT_CARBON_FIBRE).fixed()
                .put(AttachingPoints.FRONT_FRONT_SIGHT, new Vector3d(8D, 8.5101D, 4.3588D))
                .put(AttachingPoints.FRONT_RAIL_LEFT, new Vector3d(7.4118D, 7.6859D, 4.3627D))
                .put(AttachingPoints.FRONT_RAIL_RIGHT, new Vector3d(8.5876D, 7.6859D, 4.3627D))
                .put(AttachingPoints.FRONT_RAIL_BOTTOM, new Vector3d(8D, 7.1117D, 4.3627D))
                .put(AttachingPoints.FRONT_RAIL_BOTTOM_GRIP, new Vector3d(8.0D, 7.1117D, 7.0768D))),

        LONG_FRONT_STEEL_MODEL(of(LONG_FRONT_STEEL).fixed()
                .put(AttachingPoints.FRONT_FRONT_SIGHT, new Vector3d(8D, 8.3339D, 1.1389D))
                .put(AttachingPoints.FRONT_RAIL_LEFT, new Vector3d(7.3282D, 7.6581D, 1.1271D))
                .put(AttachingPoints.FRONT_RAIL_RIGHT, new Vector3d(8.6719D, 7.6581D, 1.1271D))
                .put(AttachingPoints.FRONT_RAIL_BOTTOM, new Vector3d(8D, 6.9862D, 1.1271D))
                .put(AttachingPoints.FRONT_RAIL_BOTTOM_GRIP, new Vector3d(8.0D, 6.8378D, 6.6271D))),
        LONG_FRONT_WOOD_MODEL(of(LONG_FRONT_WOOD).fixed()
                .put(AttachingPoints.FRONT_FRONT_SIGHT, new Vector3d(8D, 8.5135D, 2.5222D))
                .put(AttachingPoints.FRONT_RAIL_LEFT, new Vector3d(7.3855D, 7.7298D, 2.5431D))
                .put(AttachingPoints.FRONT_RAIL_RIGHT, new Vector3d(8.6145D, 7.7298D, 2.5431D))
                .put(AttachingPoints.FRONT_RAIL_BOTTOM, new Vector3d(8D, 6.9446D, 2.5431D))
                .put(AttachingPoints.FRONT_RAIL_BOTTOM_GRIP, new Vector3d(8.0D, 6.9154D, 6.6465D))),
        LONG_FRONT_CARBON_FIBRE_MODEL(of(LONG_FRONT_CARBON_FIBRE).fixed()
                .put(AttachingPoints.FRONT_FRONT_SIGHT, new Vector3d(8D, 8.5101D, 2.3588D))
                .put(AttachingPoints.FRONT_RAIL_LEFT, new Vector3d(7.4118D, 7.6859D, 2.3627D))
                .put(AttachingPoints.FRONT_RAIL_RIGHT, new Vector3d(8.5876D, 7.6859D, 2.3627D))
                .put(AttachingPoints.FRONT_RAIL_BOTTOM, new Vector3d(8D, 7.1117D, 2.3627D))
                .put(AttachingPoints.FRONT_RAIL_BOTTOM_GRIP, new Vector3d(8.0D, 7.1132D, 6.6662D))),

        RED_LASER_LEFT_MODEL(of(RED_LASER_LEFT).put(AttachingPoints.FRONT_RAIL_LEFT, new Vector3d(7.3855D, 7.7298D, 4.5431D))),
        GREEN_LASER_LEFT_MODEL(of(GREEN_LASER_LEFT).put(AttachingPoints.FRONT_RAIL_LEFT, new Vector3d(7.3855D, 7.7298D, 4.5431D))),
        BLUE_LASER_LEFT_MODEL(of(BLUE_LASER_LEFT).put(AttachingPoints.FRONT_RAIL_LEFT, new Vector3d(7.3855D, 7.7298D, 4.5431D))),

        RED_LASER_RIGHT_MODEL(of(RED_LASER_RIGHT).put(AttachingPoints.FRONT_RAIL_RIGHT, new Vector3d(8.6145D, 7.7298D, 4.5431D))),
        GREEN_LASER_RIGHT_MODEL(of(GREEN_LASER_RIGHT).put(AttachingPoints.FRONT_RAIL_RIGHT, new Vector3d(8.6145D, 7.7298D, 4.5431D))),
        BLUE_LASER_RIGHT_MODEL(of(BLUE_LASER_RIGHT).put(AttachingPoints.FRONT_RAIL_RIGHT, new Vector3d(8.6145D, 7.7298D, 4.5431D))),

        RED_LASER_BOTTOM_MODEL(of(RED_LASER_BOTTOM).put(AttachingPoints.FRONT_RAIL_BOTTOM, new Vector3d(8.0056D, 6.9446D, 4.5431D))),
        GREEN_LASER_BOTTOM_MODEL(of(GREEN_LASER_BOTTOM).put(AttachingPoints.FRONT_RAIL_BOTTOM, new Vector3d(8.0056D, 6.9446D, 4.5431D))),
        BLUE_LASER_BOTTOM_MODEL(of(BLUE_LASER_BOTTOM).put(AttachingPoints.FRONT_RAIL_BOTTOM, new Vector3d(8.0056D, 6.9446D, 4.5431D))),

        HANDGUARD_MODEL(of("handguard", HANDGUARD_WOOD, HANDGUARD_CARBON_FIBRE).put(AttachingPoints.FRONT_RAIL_BOTTOM_GRIP, new Vector3d(8.0D, 6.9154D, 6.6465D))),
        VERTICAL_GRIP_MODEL(of(VERTICAL_GRIP).put(AttachingPoints.FRONT_RAIL_BOTTOM_GRIP, new Vector3d(8.0D, 6.9172D, 6.1D))),
        ANGLED_GRIP_MODEL(of(ANGLED_GRIP).put(AttachingPoints.FRONT_RAIL_BOTTOM_GRIP, new Vector3d(8.0D, 6.9165D, 6.6222D))),



        CHAMBER_AUTOMATIC_MODEL(of(CHAMBER_AUTOMATIC).fixed()),
        CHAMBER_SEMI_AUTOMATIC_MODEL(of(CHAMBER_SEMI_AUTOMATIC).fixed()),
        CHAMBER_MANUAL_MODEL(of(CHAMBER_MANUAL).fixed()),

        GRIP_MODEL(of("grip", GRIP_STEEL, GRIP_WOOD, GRIP_CARBON_FIBRE).fixed()),

        MAGAZINE_WELL_SMALL_MODEL(of(MAGAZINE_WELL_SMALL).fixed()),
        MAGAZINE_WELL_MEDIUM_MODEL(of(MAGAZINE_WELL_MEDIUM).fixed()),
        MAGAZINE_WELL_LARGE_MODEL(of(MAGAZINE_WELL_LARGE).fixed()),

        MAGAZINE_SHOTGUN_MODEL(of(MAGAZINE_SHOTGUN).fixed()),
        MAGAZINE_25_CAL_MODEL(of(MAGAZINE_25_CAL).fixed()),
        MAGAZINE_40_CAL_MODEL(of(MAGAZINE_40_CAL).fixed()),
        MAGAZINE_50_CAL_MODEL(of(MAGAZINE_50_CAL).fixed()),

        SCOPE_IRON_SIGHT_MODEL(of(SCOPE_IRON_SIGHT).fixed()),
        SCOPE_RED_DOT_MODEL(of(SCOPE_RED_DOT).fixed()),
        SCOPE_SMALL_MODEL(of(SCOPE_SMALL).fixed()),
        SCOPE_MEDIUM_MODEL(of(SCOPE_MEDIUM).fixed()),
        SCOPE_LARGE_MODEL(of(SCOPE_LARGE).fixed()),

        TRIGGER_SOFT_PRESSURED_MODEL(of(TRIGGER_SOFT_PRESSURED).fixed()),
        TRIGGER_MEDIUM_PRESSURED_MODEL(of(TRIGGER_MEDIUM_PRESSURED).fixed()),
        TRIGGER_HEAVY_PRESSURED_MODEL(of(TRIGGER_HEAVY_PRESSURED).fixed()),

        STOCK_STEEL_MODEL(of(STOCK_STEEL).fixed()
                .put(AttachingPoints.STOCK_STOCK_TOP, new Vector3d(8.0D, 8.2694D, 15.9477D))
                .put(AttachingPoints.STOCK_BUTT, new Vector3d(8.0D, 6.699D, 17.7524D))),
        STOCK_WOOD_MODEL(of(STOCK_WOOD).fixed()
                .put(AttachingPoints.STOCK_STOCK_TOP, new Vector3d(8.0D, 8.1052D, 16.5173D))
                .put(AttachingPoints.STOCK_BUTT, new Vector3d(8.0D, 6.6912D, 18.572D))),
        STOCK_CARBON_FIBRE_MODEL(of(STOCK_CARBON_FIBRE).fixed()
                .put(AttachingPoints.STOCK_STOCK_TOP, new Vector3d(8.0D, 7.5105D, 16.9951D))
                .put(AttachingPoints.STOCK_BUTT, new Vector3d(8.0D, 6.5162D, 18.1094D))),

        STOCK_TOP_STEEL_MODEL(of(STOCK_TOP_STEEL).put(AttachingPoints.STOCK_STOCK_TOP, new Vector3d(8.0D, 7.5105D, 16.9951D))),
        STOCK_TOP_WOOD_MODEL(of(STOCK_TOP_WOOD).put(AttachingPoints.STOCK_STOCK_TOP, new Vector3d(8.0D, 7.5105D, 16.9951D))),
        STOCK_TOP_CARBON_FIBRE_MODEL(of(STOCK_TOP_CARBON_FIBRE).put(AttachingPoints.STOCK_STOCK_TOP, new Vector3d(8.0D, 7.5105D, 16.9951D))),

        BUTT_STEEL_MODEL(of(BUTT_STEEL).put(AttachingPoints.STOCK_BUTT, new Vector3d(8.0D, 6.5235D, 18.2555D))),
        BUTT_WOOD_MODEL(of(BUTT_WOOD).put(AttachingPoints.STOCK_BUTT, new Vector3d(8.0D, 6.5162D, 18.1055D))),
        BUTT_SILICON_MODEL(of(BUTT_SILICON).put(AttachingPoints.STOCK_BUTT, new Vector3d(8.0D, 6.574D, 18.1078D))),
        ;
        private final String modelName;
        private final List<GunComponents> components;
        private final HashMap<AttachingPoints, Vector3d> points = new HashMap<>();
        private final boolean isFixed;

        GunComponentModels(Builder builder)
        {
            this(builder.name, builder.components, builder.isFixed);
            this.points.putAll(builder.points);
        }

        GunComponentModels(String modelName, List<GunComponents> components, boolean isFixed)
        {
            this.modelName = modelName;
            this.components = components;
            this.isFixed = isFixed;
        }

        @Override
        public String toString()
        {
            return "GunComponentModels{" +
                    "modelName='" + modelName + '\'' +
                    ", components=" + components +
                    '}';
        }

        public ModelResourceLocation getModelLocation()
        {
            return new ModelResourceLocation(AssembleGuns.MODID + ":" + modelName + "#inventory");
        }

        public static GunComponentModels fromComponent(GunComponentItem component)
        {
            return Arrays.stream(GunComponentModels.values()).filter((model) -> model.components.contains(component.component)).findFirst().orElse(GunComponentModels.GRIP_MODEL);
        }

        public String getModelName()
        {
            return modelName;
        }

        public List<GunComponents> getComponents()
        {
            return components;
        }

        public HashMap<AttachingPoints, Vector3d> getPoints()
        {
            return points;
        }

        public boolean isFixed()
        {
            return isFixed;
        }

        private static Builder of(GunComponents... components)
        {
            return new Builder(components);
        }

        private static Builder of(String name, GunComponents... components)
        {
            return new Builder(name, components);
        }

        static class Builder
        {
            private final String name;
            private final List<GunComponents> components;
            private final HashMap<AttachingPoints, Vector3d> points = new HashMap<>();
            private boolean isFixed = false;

            private Builder(GunComponents... components)
            {
                this(components[0].name().toLowerCase(Locale.ROOT), components);
            }

            private Builder(String name, GunComponents... components)
            {
                this.name = name;
                this.components = Arrays.asList(components);
            }

            public Builder fixed()
            {
                isFixed = true;
                return this;
            }

            public Builder put(AttachingPoints point, Vector3d vec)
            {
                points.put(point, vec);
                return this;
            }
        }
    }

    public enum ScopeTypes implements AVAItemGun.IScopeType
    {
        EMPTY()
        ;

        ScopeTypes()
        {
        }

        @Nullable
        @Override
        public ResourceLocation getTexture()
        {
            return null;
        }

        @Override
        public float getMagnification(ItemStack stack)
        {
            return AssembleGunItem.getTotal(stack, MAGNIFICATION);
        }

        @Override
        public boolean noAim(ItemStack stack)
        {
            return !GunComponentsManager.getCap(stack).isPartInstalled(SCOPE);
        }
    }
}
