package dice.assembleguns.component.components;

import dice.assembleguns.component.GunParts;
import dice.assembleguns.component.components.parts.GunPart;
import dice.assembleguns.items.GunComponentItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.LazyOptional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

public class GunComponentsManager implements ICapabilitySerializable<CompoundNBT>, IGunComponentsManager
{
    @CapabilityInject(IGunComponentsManager.class)
    public static Capability<IGunComponentsManager> MANAGER_CAPABILITY = null;

    private final LazyOptional<IGunComponentsManager> lazyOptional = LazyOptional.of(GunComponentsManager::new);

    public final List<GunPart> parts = new ArrayList<>();
    private final HashMap<String, GunPart> keys = new HashMap<>();

    public final GunPart barrel = addPart(GunParts.BARREL);
    public final GunPart frontSight = addPart(GunParts.FRONT_SIGHT);
    public final GunPart front = addPart(GunParts.HANDGUARD);
    public final GunPart muzzle = addPart(GunParts.MUZZLE);
    public final GunPart railLeft = addPart(GunParts.RAIL_LEFT);
    public final GunPart railRight = addPart(GunParts.RAIL_RIGHT);
    public final GunPart railBottom = addPart(GunParts.RAIL_BOTTOM);
    public final GunPart railBottomGrip = addPart(GunParts.RAIL_BOTTOM_GRIP);

    public final GunPart chamber = addPart(GunParts.CHAMBER);
    public final GunPart grip = addPart(GunParts.GRIP);
    public final GunPart magazine_well = addPart(GunParts.MAGAZINE_WELL);
    public final GunPart magazine = addPart(GunParts.MAGAZINE);
    public final GunPart scope = addPart(GunParts.SCOPE);
    public final GunPart trigger = addPart(GunParts.TRIGGER);

    public final GunPart stock = addPart(GunParts.STOCK);
    public final GunPart stockTop = addPart(GunParts.STOCK_TOP);
    public final GunPart butt = addPart(GunParts.BUTT);

    private GunPart addPart(GunParts p)
    {
        GunPart part = new GunPart(p);
        parts.add(part);
        keys.put(p.name(), part);
        return part;
    }

    public float sum(GunComponents.ComponentAttributes attribute)
    {
        AtomicReference<Float> value = new AtomicReference<>(0.0F);
        for (GunPart part : getParts())
            part.getComponentItem().ifPresent((component) -> value.updateAndGet((i) -> i + component.component.getAttributesMap().get(attribute)));
        return value.get();
    }

    public static GunComponentsManager getCap(ItemStack stack)
    {
        return (GunComponentsManager) stack.getCapability(MANAGER_CAPABILITY).orElseThrow(NullPointerException::new);
    }

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side)
    {
        return cap == MANAGER_CAPABILITY ? lazyOptional.cast() : LazyOptional.empty();
    }

    @Override
    public CompoundNBT serializeNBT()
    {
        return (CompoundNBT) MANAGER_CAPABILITY.getStorage().writeNBT(MANAGER_CAPABILITY, lazyOptional.orElseThrow(() -> new NullPointerException("An error has occur during capability io")), null);
    }

    @Override
    public void deserializeNBT(CompoundNBT nbt)
    {
        MANAGER_CAPABILITY.getStorage().readNBT(MANAGER_CAPABILITY, lazyOptional.orElseThrow(() -> new NullPointerException("An error has occur during capability io")), null, nbt);
    }

    @Override
    public List<GunPart> getParts()
    {
        return new ArrayList<>(parts);
    }

    @Override
    public HashMap<String, GunPart> getKeys()
    {
        return new HashMap<>(keys);
    }

    public GunPart getPart(GunParts part)
    {
        return getKeys().get(part.name());
    }

    @Override
    public boolean isInstalled(GunComponents component)
    {
        GunPart part = getKeys().get(component.getCategory().name());
        if (!part.getComponentItem().isPresent())
            return false;
        return ((GunComponentItem) part.getComponentStack().getItem()).component == component;
    }

    @Override
    public boolean isPartInstalled(GunParts part)
    {
        return getKeys().get(part.name()).installed();
    }

    @Override
    public void install(GunComponents component)
    {
        GunPart part = getKeys().get(component.getCategory().name());
        part.setComponent(component.asItemStack());
        validateComponents();
    }

    @Override
    public void unInstall(GunComponents component)
    {
        GunPart part = getKeys().get(component.getCategory().name());
        part.setComponent(ItemStack.EMPTY);
        validateComponents();
    }

    public void validateComponents()
    {
        getInstalledParts().forEach((part) -> {
            if (!isComponentValid(part.getComponentItem().get().component))
                part.setComponent(ItemStack.EMPTY);
        });
    }

    public boolean isComponentValid(GunComponents component)
    {
        for (GunParts req : component.getCategory().getRequirements())
            if (!isPartInstalled(req))
                return false;
        for (GunComponents req : component.getRequirements())
            if (!isInstalled(req))
                return false;
        return true;
    }

    public Stream<GunPart> getInstalledParts()
    {
        return getParts().stream().filter(GunPart::installed);
    }

    public static class Storage implements Capability.IStorage<IGunComponentsManager>
    {
        public Storage(){}

        @Nullable
        @Override
        public INBT writeNBT(Capability<IGunComponentsManager> capability, IGunComponentsManager instance, Direction side)
        {
            CompoundNBT compound = new CompoundNBT();
            ListNBT list = new ListNBT();
            instance.getParts().forEach((part) -> list.add(part.getComponentHolder().serializeNBT()));
            compound.put("list", list);
            return compound;
        }

        @Override
        public void readNBT(Capability<IGunComponentsManager> capability, IGunComponentsManager instance, Direction side, INBT nbt)
        {
            CompoundNBT compound = (CompoundNBT) nbt;
            ListNBT list = compound.getList("list", Constants.NBT.TAG_COMPOUND);
            for (int i = 0; i < instance.getParts().size(); i++)
                instance.getParts().get(i).getComponentHolder().deserializeNBT(list.getCompound(i));
        }
    }
}
