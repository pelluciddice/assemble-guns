package dice.assembleguns.component.components.parts;

import dice.assembleguns.component.GunParts;
import dice.assembleguns.items.GunComponentItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nullable;
import java.util.Optional;

public class GunPart
{
    protected final GunParts part;
    protected ItemStackHandler component = new ItemStackHandler(1);

    public GunPart(GunParts part)
    {
        this.part = part;
        component.setStackInSlot(0,
                part.hasDefault() ? part.getDefault().asItemStack() :
                ItemStack.EMPTY
        );
    }

    public String getName()
    {
        return part.getName();
    }

    public ItemStackHandler getComponentHolder()
    {
        return component;
    }

    public Optional<GunComponentItem> getComponentItem()
    {
        return installed() ? Optional.of((GunComponentItem) getComponentStack().getItem()) : Optional.empty();
    }

    public ItemStack getComponentStack()
    {
        return component.getStackInSlot(0);
    }

    public void setComponent(ItemStack stack)
    {
        component.setStackInSlot(0, stack);
    }

    public boolean installed()
    {
        ItemStack stack = getComponentStack();
        if (stack.isEmpty())
            return false;
        if (stack.getItem() instanceof GunComponentItem)
            return true;
        setComponent(ItemStack.EMPTY);
        return false;
    }

    public GunParts getPart()
    {
        return part;
    }

    @Override
    public String toString()
    {
        return "GunPart{" +
                "part=" + part +
                ", component=" + component.getStackInSlot(0) +
                '}';
    }
}
