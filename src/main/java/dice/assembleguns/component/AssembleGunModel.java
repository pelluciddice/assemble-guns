package dice.assembleguns.component;

import com.mojang.blaze3d.matrix.MatrixStack;
import dice.assembleguns.component.components.GunComponents;
import dice.assembleguns.component.components.GunComponentsManager;
import dice.assembleguns.component.components.parts.GunPart;
import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.model.BakedQuad;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.math.vector.TransformationMatrix;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.client.model.ModelLoader;
import pellucid.ava.misc.renderers.Animation;
import pellucid.ava.misc.renderers.Perspective;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;

import javax.annotation.Nullable;
import java.util.*;
import java.util.function.Consumer;

public class AssembleGunModel extends ModifiedGunModel
{
    private static final Perspective ORIGINAL_FP_RIGHT = new Perspective(
            new Vector3f(1, 1, 0),
            new Vector3f(-5.75F, 5.5F, 5.5F),
            new Vector3f(1, 1F, 0.5F));


    private final GunComponentsManager manager;
    public AssembleGunModel(IBakedModel origin, ItemStack stack, @Nullable ClientWorld world, @Nullable LivingEntity entity)
    {
        super(origin, stack, world, entity);
        manager = GunComponentsManager.getCap(stack);
    }

    private static final ModelResourceLocation SLIDE = new ModelResourceLocation("assemble_guns:slide#inventory");
    private static final ModelResourceLocation HANDLE = new ModelResourceLocation("assemble_guns:handle#inventory");
    private static final ModelResourceLocation FIRE = new ModelResourceLocation("assemble_guns:fire#inventory");

    public static void addSpecialModels()
    {
        ModelLoader.addSpecialModel(SLIDE);
        ModelLoader.addSpecialModel(HANDLE);
        ModelLoader.addSpecialModel(FIRE);
    }

    private static final HashMap<GunComponents, Perspective> AIMING_SCOPE = new HashMap<GunComponents, Perspective>() {{
        put(GunComponents.SCOPE_IRON_SIGHT, new Perspective(new float[]{6, 0, 0}, new float[]{-9.012F, 7.27F, 8.75F}, new float[]{1.0F, 1.0F, 0.5F}));
        put(GunComponents.SCOPE_RED_DOT, new Perspective(new float[]{3, 0, 0}, new float[]{-9.0165F, 6.639F, 8.75F}, new float[]{1.25F, 1.25F, 0.5F}));
        put(GunComponents.SCOPE_SMALL, new Perspective(new float[]{3, 0, 0}, new float[]{-8.9975F, 5.7725F, 9.5F}, new float[]{1.5F, 1.5F, 0.2F}));
        put(GunComponents.SCOPE_MEDIUM, new Perspective(new float[]{3, 0, 0}, new float[]{-8.999F, 4.6825F, 10F}, new float[]{2.0F, 2.0F, 0.1F}));
        put(GunComponents.SCOPE_LARGE,new Perspective(new float[]{3, 0, 0}, new float[]{-8.9975F, 5.055F, 10.25F}, new float[]{2.5F, 2.5F, 0.05F}));
    }};

    @Override
    public Perspective getAimingPosition()
    {
        GunPart part = manager.getPart(GunParts.SCOPE);
        if (part.installed())
            return AIMING_SCOPE.get(part.getComponentItem().get().component);
        return null;
    }

    private static final Perspective LEFT_HAND_IDLE_NONE = new Perspective(-11.0F, 0.0F, 50.0F, 0.2F, -0.55F, -0.05F, 0.55F, 1.0F, 0.55F);
    private static final Perspective LEFT_HAND_IDLE_SHORT = new Perspective(-12.0F, 0.0F, 39.0F, 0.075F, -0.575F, -0.025F, 0.55F, 1.0F, 0.55F);
    private static final Perspective LEFT_HAND_IDLE_LONG = new Perspective(-11.0F, 7.0F, 33.0F, 0.0F, -0.575F, -0.05F, 0.55F, 1.0F, 0.55F);
    public static Perspective getLeftHandIdle(ItemStack stack)
    {
        GunComponentsManager manager = GunComponentsManager.getCap(stack);
        if (!manager.isPartInstalled(GunParts.HANDGUARD))
            return LEFT_HAND_IDLE_NONE;
        else if (!GunComponents.isLongFront(manager.getPart(GunParts.HANDGUARD).getComponentItem().get().component))
            return LEFT_HAND_IDLE_SHORT;
        return LEFT_HAND_IDLE_LONG;
    }

    @Override
    public List<BakedQuad> getQuads(@Nullable BlockState state, @Nullable Direction side, Random rand)
    {
        List<BakedQuad> quads = new ArrayList<>();
        if (side != null)
            return quads;
        int colour = stack.getOrCreateTag().getInt("colour");
        quads.addAll(modifyColours(origin.getQuads(state, side, rand), colour));
        for (GunPart managePart : manager.getParts())
        {
            managePart.getComponentItem().ifPresent((item) -> {
                GunComponents.GunComponentModels model = GunComponents.GunComponentModels.fromComponent(item);
                Consumer<BakedQuad> consumer = (quad) -> {};
                GunComponents component = item.component;
                if (!model.isFixed())
                {
                    GunParts category = component.getCategory();
                    GunComponents.AttachingPoints point = GunComponents.AttachingPoints.getFromSecondPart(category);
                    Vector3d vec = model.getPoints().get(point);

                    GunParts part = point.getConnected()[0];
                    GunPart managePart2 = manager.getKeys().get(part.name());

                    if (managePart2.getComponentItem().isPresent())
                    {
                        GunComponents.GunComponentModels model2 = GunComponents.GunComponentModels.fromComponent(managePart2.getComponentItem().get());
                        Vector3d vec2 = model2.getPoints().get(point);
                        vec2 = vec2.subtract(vec);
                        Vector3d finalVec = vec2;
                        consumer = (quad) ->
                        {
                            translateQuad(quad, finalVec);
                        };
                    }
                }
                List<BakedQuad> componentQuads = get(model.getModelLocation(), consumer);
                if (component.isRecolourable())
                    componentQuads = modifyColours(componentQuads, colour);
                quads.addAll(componentQuads);
            });
        }
        quads.addAll(modifyColours(getSlideQuads(), colour));
        quads.addAll(modifyColours(getHandleQuads(), colour));
        quads.addAll(getFireQuads(state, rand));
        return quads;
    }

    @Override
    public IBakedModel handlePerspective(ItemCameraTransforms.TransformType type, MatrixStack mat)
    {
        Vector3f rotation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f translation = new Vector3f(0.0F, 0.0F, 0.0F);
        Vector3f scale = new Vector3f(1.0F, 1.0F, 1.0F);
        switch (type)
        {
            case THIRD_PERSON_LEFT_HAND:
            case THIRD_PERSON_RIGHT_HAND:
                rotation = new Vector3f(70, 0, 0);
                translation = new Vector3f(-2F, 3.75F, 4.25F);
                scale = v3f(1.15F);
                break;
            case FIRST_PERSON_LEFT_HAND:
            case FIRST_PERSON_RIGHT_HAND:
                rotation = ORIGINAL_FP_RIGHT.rotation.copy();
                translation = ORIGINAL_FP_RIGHT.translation.copy();
                scale = ORIGINAL_FP_RIGHT.scale.copy();
                break;
            case GROUND:
                rotation = new Vector3f(0.0F, 0.0F, 90.0F);
                scale = v3f(0.75F);
                break;
            case GUI:
                rotation = new Vector3f(0, -90, 0);
                scale = v3f(0.7F);
                break;
            case FIXED:
                rotation = new Vector3f(0, -90, 0);
                translation = new Vector3f(0.5F, 0.025F, -1.25F);
                scale = v3f(1.5F);
                break;
        }
        this.modifyPerspective(rotation, translation, scale, type);
        TransformationMatrix transformationMatrix = this.getTransformationMatrix(rotation, translation, scale);
        if (!transformationMatrix.isIdentity())
            transformationMatrix.push(mat);
        return getBakedModel();
    }

    @Override
    protected ModelResourceLocation getFireModel()
    {
        return FIRE;
    }

    @Override
    protected List<BakedQuad> getFireQuads(@Nullable BlockState state, Random rand)
    {
        if (getFireModel() == null || fire == 0 || fire > 2 || !manager.barrel.installed())
            return Collections.emptyList();
        return get(getFireModel(), (quad) -> {
            float zPos;
            if (GunComponents.isLongBarrel(manager.barrel.getComponentItem().get().component))
            {
                if (manager.isPartInstalled(GunParts.MUZZLE))
                    zPos = -2.8434F;
                else
                    zPos = -1.6051F;
            }
            else
            {
                if (manager.isPartInstalled(GunParts.MUZZLE))
                    zPos = 1.7502F;
                else
                    zPos = 2.9885F;
            }
            translateQuad(quad, zPos + 2.8434F, Direction.SOUTH);
        }, manager.isInstalled(GunComponents.SUPPRESSOR));
    }

    private static final float[] RUN_ROTATION = new float[]{-35, 34, 27};
    private static final Perspective RUN_1 = new Perspective(RUN_ROTATION, new float[]{-9.75F, 3.25F, 4.5F}, ONE_A);
    private static final Perspective RUN_2 = new Perspective(RUN_ROTATION, new float[]{-9.25F, 2.75F, 4.5F}, ONE_A);
    protected static final ArrayList<Animation> RUN_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RUN_1));
        add(new Animation(3, RUN_2));
        add(new Animation(6, new Perspective(RUN_ROTATION, new float[]{-8.75F, 3.25F, 4.5F}, ONE_A)));
        add(new Animation(9, RUN_2));
        add(new Animation(12, RUN_1));
    }};

    @Override
    protected ArrayList<Animation> getRunAnimation()
    {
        return RUN_ANIMATION;
    }

    private static final Perspective RELOAD_LOWER_GUN = new Perspective(new float[]{30, 18, 34}, new float[]{-7.5F, 6.25F, 5.5F}, new float[]{1, 1, 0.5F});
    private static final Perspective RELOAD_LOWER_GUN_EXTRACT_MAG = new Perspective(new float[]{18, 14, 34}, new float[]{-7.5F, 6F, 5.5F}, new float[]{1, 1, 0.5F});
    private static final Perspective RELOAD_LOWER_GUN_INSERT_MAG = new Perspective(new float[]{30, 18, 36}, new float[]{-7.75F, 6.5F, 5.5F}, new float[]{1, 1, 0.5F});

    private static final List<Animation> RELOAD_ANIMATION_COMMON = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(4, RELOAD_LOWER_GUN));
        add(new Animation(5, RELOAD_LOWER_GUN));
        add(new Animation(7, RELOAD_LOWER_GUN_EXTRACT_MAG));
        add(new Animation(8, RELOAD_LOWER_GUN_EXTRACT_MAG));
        add(new Animation(14, RELOAD_LOWER_GUN));
        add(new Animation(22, RELOAD_LOWER_GUN));
        add(new Animation(24, RELOAD_LOWER_GUN_INSERT_MAG));
        add(new Animation(25, RELOAD_LOWER_GUN_INSERT_MAG));
    }};

    private static final Perspective RELOAD_SEMI_AUTO_SLIDE = new Perspective(new float[]{-14, -1, 20}, new float[]{-7.5F, 5.75F, 6.5F}, new float[]{1, 1, 0.5F});
    private static final Perspective RELOAD_SEMI_AUTO_SLIDE_PULL = new Perspective(new float[]{6, -1, 20}, new float[]{-7.5F, 6.25F, 6.5F}, new float[]{1, 1, 0.5F});
    private static final Perspective RELOAD_SEMI_AUTO_SLIDE_RELEASE = new Perspective(new float[]{-10, -1, 20}, new float[]{-7.5F, 6.5F, 6.25F}, new float[]{1, 1, 0.5F});

    private static final Perspective RELOAD_MANUAL_HANDLE = new Perspective(new float[]{4, 4, 29}, new float[]{-7.5F, 6.75F, 6.5F}, new float[]{1, 1, 0.5F});
    private static final Perspective RELOAD_MANUAL_HANDLE_PULL = new Perspective(new float[]{8, 8, 29}, new float[]{-7.5F, 6.5F, 6.75F}, new float[]{1, 1, 0.5F});
    private static final Perspective RELOAD_MANUAL_HANDLE_PUSH = new Perspective(new float[]{-4, -1, 29}, new float[]{-7.5F, 6.25F, 6F}, new float[]{1, 1, 0.5F});

    private static final HashMap<GunComponents, ArrayList<Animation>> RELOAD_ANIMATION = new HashMap<GunComponents, ArrayList<Animation>>() {{
        put(GunComponents.CHAMBER_AUTOMATIC, new ArrayList<Animation>() {{
            addAll(RELOAD_ANIMATION_COMMON);
            add(new Animation(29, ORIGINAL_FP_RIGHT));
            add(new Animation(30, ORIGINAL_FP_RIGHT));
        }});
        put(GunComponents.CHAMBER_SEMI_AUTOMATIC, new ArrayList<Animation>() {{
            addAll(RELOAD_ANIMATION_COMMON);
            add(new Animation(26, RELOAD_LOWER_GUN_INSERT_MAG));
            add(new Animation(30, RELOAD_SEMI_AUTO_SLIDE));
            add(new Animation(32, RELOAD_SEMI_AUTO_SLIDE));
            add(new Animation(35, RELOAD_SEMI_AUTO_SLIDE_PULL));
            add(new Animation(38, RELOAD_SEMI_AUTO_SLIDE_RELEASE));
            add(new Animation(42, ORIGINAL_FP_RIGHT));
        }});
        put(GunComponents.CHAMBER_MANUAL, new ArrayList<Animation>() {{
            addAll(RELOAD_ANIMATION_COMMON);
            add(new Animation(26, RELOAD_LOWER_GUN_INSERT_MAG));
            add(new Animation(30, RELOAD_MANUAL_HANDLE));
            add(new Animation(34, RELOAD_MANUAL_HANDLE_PULL));
            add(new Animation(35, RELOAD_MANUAL_HANDLE_PULL));
            add(new Animation(38, RELOAD_MANUAL_HANDLE_PUSH));
            add(new Animation(42, ORIGINAL_FP_RIGHT));
        }});
    }};

    @Override
    protected ArrayList<Animation> getReloadAnimation()
    {
        return RELOAD_ANIMATION.get(manager.chamber.getComponentItem().get().component);
    }

    protected List<BakedQuad> getSlideQuads()
    {
        if (manager.isInstalled(GunComponents.CHAMBER_SEMI_AUTOMATIC))
        {
            return get(SLIDE, (quad) -> {
                if (!translateQuadTo(quad, 2.0F, reload, 33, 35, Direction.SOUTH))
                    if (!translateQuad(quad, 2.0F, reload, 35, 36, Direction.SOUTH))
                        translateQuadFrom(quad, 2.0F, reload, 36, 38, Direction.NORTH);
            });
        }
        return Collections.emptyList();
    }

    protected List<BakedQuad> getHandleQuads()
    {
        if (manager.isInstalled(GunComponents.CHAMBER_MANUAL))
        {
            return get(HANDLE, (quad) -> {
                if (!translateQuadTo(quad, 1.0F, reload, 30, 33, Direction.SOUTH))
                    if (!translateQuad(quad, 1.0F, reload, 33, 34, Direction.SOUTH))
                        translateQuadFrom(quad, 1.0F, reload, 34, 36, Direction.NORTH);
                if (!translateQuadTo(quad, 1.0F, fire, 11, 13, Direction.SOUTH))
                    if (!translateQuad(quad, 1.0F, fire, 13, 14, Direction.SOUTH))
                        translateQuadFrom(quad, 1.0F, fire, 14, 17, Direction.NORTH);
            });
        }
        return Collections.emptyList();
    }

    private static final Perspective LH_RELOAD_LOWER_GUN = new Perspective(-20.0F, -18.0F, 23.0F, -0.075F, -0.625F, 0.05F, 0.55F, 1.0F, 0.55F);
    private static final Perspective LH_RELOAD_LOWER_GUN_EXTRACT_MAG = new Perspective(-20.0F, -18.0F, 21.0F, -0.125F, -0.625F, 0.075F, 0.55F, 1.0F, 0.55F);
    private static final Perspective LH_RELOAD_LOWER_GUN_INSERT_MAG = new Perspective(-20.0F, -18.0F, 19.0F, -0.125F, -0.6F, 0.025F, 0.55F, 1.0F, 0.55F);
    private static final Perspective LH_RELOAD_HOLD = new Perspective(-25.0F, -18.0F, 14.0F, -0.175F, -0.575F, 0.125F, 0.55F, 1.0F, 0.55F);
    private static final Perspective LH_RELOAD_HOLD_2 = new Perspective(-30.0F, -18.0F, 14.0F, -0.225F, -0.625F, 0.175F, 0.55F, 1.0F, 0.55F);

    private static final List<Animation> LH_RELOAD_ANIMATION_COMMON = new ArrayList<Animation>() {{
        add(new Animation(0, Perspective.EMPTY));
        add(new Animation(4, LH_RELOAD_LOWER_GUN));
        add(new Animation(5, LH_RELOAD_LOWER_GUN));
        add(new Animation(7, LH_RELOAD_LOWER_GUN_EXTRACT_MAG));
        add(new Animation(8, LH_RELOAD_LOWER_GUN_EXTRACT_MAG));
        add(new Animation(14, LH_RELOAD_LOWER_GUN));
        add(new Animation(22, LH_RELOAD_LOWER_GUN));
        add(new Animation(24, LH_RELOAD_LOWER_GUN_INSERT_MAG));
        add(new Animation(25, LH_RELOAD_LOWER_GUN_INSERT_MAG));
    }};


    private static final HashMap<GunComponents, ArrayList<Animation>> LH_RELOAD_ANIMATION = new HashMap<GunComponents, ArrayList<Animation>>() {{
        put(GunComponents.CHAMBER_AUTOMATIC, new ArrayList<Animation>() {{
            addAll(LH_RELOAD_ANIMATION_COMMON);
            add(new Animation(27, LH_RELOAD_HOLD));
            add(new Animation(30, Perspective.EMPTY));
        }});
        put(GunComponents.CHAMBER_SEMI_AUTOMATIC, new ArrayList<Animation>() {{
            addAll(LH_RELOAD_ANIMATION_COMMON);
            add(new Animation(26, LH_RELOAD_HOLD));
            add(new Animation(38, LH_RELOAD_HOLD));
            add(new Animation(42, Perspective.EMPTY));
        }});
        put(GunComponents.CHAMBER_MANUAL, new ArrayList<Animation>() {{
            addAll(LH_RELOAD_ANIMATION_COMMON);
            add(new Animation(26, LH_RELOAD_HOLD));
            add(new Animation(30, LH_RELOAD_HOLD));
            add(new Animation(34, LH_RELOAD_HOLD_2));
            add(new Animation(35, LH_RELOAD_HOLD_2));
            add(new Animation(38, LH_RELOAD_HOLD_2));
            add(new Animation(42, Perspective.EMPTY));
        }});
    }};

    public static List<Animation> getReloadAnimationLeftHand(ItemStack stack)
    {
        return replaceEmptyWithDefault(LH_RELOAD_ANIMATION.get(GunComponentsManager.getCap(stack).chamber.getComponentItem().get().component), getLeftHandIdle(stack));
    }

    private static final Perspective RH_ORIGINAL = new Perspective(-1.0F, -10.0F, 3.0F, 0.025F, -0.775F, -0.175F, 0.425F, 1.0F, 0.425F);

    private static final List<Animation> RH_RELOAD_ANIMATION_COMMON = new ArrayList<Animation>() {{
        add(new Animation(0, RH_ORIGINAL));
        add(new Animation(25, RH_ORIGINAL));
    }};

    private static final Perspective RH_SLIDE_HOLD = new Perspective(-7.0F, -21.0F, -9.0F, -0.025F, -0.85F, -0.2F, 0.425F, 1.0F, 0.45F);
    private static final Perspective RH_SLIDE_PULL = new Perspective(-10.0F, -21.0F, -9.0F, -0.025F, -0.875F, -0.15F, 0.425F, 1.0F, 0.45F);

    private static final Perspective RH_HANDLE_HOLD = new Perspective(-18.0F, -21.0F, -9.0F, -0.125F, -0.775F, -0.15F, 0.425F, 1.0F, 0.425F);
    private static final Perspective RH_HANDLE_PULL = new Perspective(-18.0F, -21.0F, -9.0F, -0.125F, -0.825F, -0.15F, 0.425F, 1.0F, 0.425F);

    private static final HashMap<GunComponents, ArrayList<Animation>> RH_RELOAD_ANIMATION = new HashMap<GunComponents, ArrayList<Animation>>() {{
        put(GunComponents.CHAMBER_AUTOMATIC, new ArrayList<Animation>() {{
            addAll(RH_RELOAD_ANIMATION_COMMON);
            add(new Animation(30, RH_ORIGINAL));
        }});
        put(GunComponents.CHAMBER_SEMI_AUTOMATIC, new ArrayList<Animation>() {{
            addAll(RH_RELOAD_ANIMATION_COMMON);
            add(new Animation(31, RH_SLIDE_HOLD));
            add(new Animation(34, RH_SLIDE_PULL));
            add(new Animation(37, RH_SLIDE_HOLD));
            add(new Animation(41, RH_ORIGINAL));
            add(new Animation(42, RH_ORIGINAL));
        }});
        put(GunComponents.CHAMBER_MANUAL, new ArrayList<Animation>() {{
            addAll(RH_RELOAD_ANIMATION_COMMON);
            add(new Animation(31, RH_HANDLE_HOLD));
            add(new Animation(34, RH_HANDLE_PULL));
            add(new Animation(37, RH_HANDLE_HOLD));
            add(new Animation(41, RH_ORIGINAL));
            add(new Animation(42, RH_ORIGINAL));
        }});
    }};

    public static List<Animation> getReloadAnimationRightHand(ItemStack stack)
    {
        return RH_RELOAD_ANIMATION.get(GunComponentsManager.getCap(stack).chamber.getComponentItem().get().component);
    }

    private static final ArrayList<Animation> LH_FIRE_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, Perspective.EMPTY));
        add(new Animation(6, Perspective.EMPTY));
        add(new Animation(11, LH_RELOAD_HOLD_2));
        add(new Animation(17, LH_RELOAD_HOLD_2));
        add(new Animation(20, Perspective.EMPTY));
    }};

    public static List<Animation> getFireAnimationLeftHand(ItemStack stack)
    {
        if (GunComponentsManager.getCap(stack).isInstalled(GunComponents.CHAMBER_MANUAL))
            return replaceEmptyWithDefault(LH_FIRE_ANIMATION, getLeftHandIdle(stack));
        return Collections.emptyList();
    }

    private static final ArrayList<Animation> RH_FIRE_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, RH_ORIGINAL));
        add(new Animation(6, RH_ORIGINAL));
        add(new Animation(11, RH_HANDLE_HOLD));
        add(new Animation(14, RH_HANDLE_PULL));
        add(new Animation(17, RH_HANDLE_HOLD));
        add(new Animation(20, RH_ORIGINAL));
    }};

    public static List<Animation> getFireAnimationRightHand(ItemStack stack)
    {
        if (GunComponentsManager.getCap(stack).isInstalled(GunComponents.CHAMBER_MANUAL))
            return RH_FIRE_ANIMATION;
        return Collections.emptyList();
    }

    private static final ArrayList<Animation> FIRE_ANIMATION = new ArrayList<Animation>() {{
        add(new Animation(0, ORIGINAL_FP_RIGHT));
        add(new Animation(6, ORIGINAL_FP_RIGHT));
        add(new Animation(11, RELOAD_MANUAL_HANDLE_PUSH));
        add(new Animation(13, RELOAD_MANUAL_HANDLE_PULL));
        add(new Animation(14, RELOAD_MANUAL_HANDLE_PULL));
        add(new Animation(17, RELOAD_MANUAL_HANDLE_PUSH));
        add(new Animation(20, ORIGINAL_FP_RIGHT));
    }};

    @Override
    public void modifyPerspective(Vector3f rotation, Vector3f translation, Vector3f scale, ItemCameraTransforms.TransformType type)
    {
        if (fire != 0)
        {
            Perspective perspective3f = getPerspectiveInBetween(FIRE_ANIMATION, fire);
            rotation.set(perspective3f.getRotation());
            translation.set(perspective3f.getTranslation());
            scale.set(perspective3f.getScale());
        }
        super.modifyPerspective(rotation, translation, scale, type, false);
    }

    private static List<Animation> replaceEmptyWithDefault(List<Animation> animations, Perspective defaultPerspective)
    {
        for (int i = 0; i < animations.size(); i++)
        {
            Animation animation = animations.get(i);
            if (animation.target3f.isEmpty())
                animations.set(i, new Animation(animation.targetTicks, defaultPerspective));
        }
        return animations;
    }
}
