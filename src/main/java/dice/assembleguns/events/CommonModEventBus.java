package dice.assembleguns.events;

import dice.assembleguns.component.components.GunComponentsManager;
import dice.assembleguns.component.components.IGunComponentsManager;
import dice.assembleguns.items.ModItems;
import dice.assembleguns.packets.ModPackets;
import net.minecraft.item.Item;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class CommonModEventBus
{
    @SubscribeEvent
    public static void onSetup(FMLCommonSetupEvent event)
    {
        CapabilityManager.INSTANCE.register(IGunComponentsManager.class, new GunComponentsManager.Storage(), GunComponentsManager::new);
        ModPackets.registerAll();
    }

    @SubscribeEvent
    public static void onItemsRegister(RegistryEvent.Register<Item> event)
    {
        ModItems.registerAll(event.getRegistry());
    }
}
