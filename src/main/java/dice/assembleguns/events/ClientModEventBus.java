package dice.assembleguns.events;

import dice.assembleguns.component.AssembleGunModel;
import dice.assembleguns.component.TestModel;
import dice.assembleguns.component.components.GunComponents;
import dice.assembleguns.items.ModItems;
import dice.assembleguns.packets.ColourMessage;
import net.minecraft.client.renderer.model.IBakedModel;
import net.minecraft.client.renderer.model.ModelResourceLocation;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ModelBakeEvent;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import pellucid.ava.items.miscs.ICustomModel;
import pellucid.ava.misc.renderers.Model;
import pellucid.ava.misc.renderers.models.ModifiedGunModel;
import pellucid.ava.util.AVAClientUtil;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientModEventBus
{
    @SubscribeEvent
    public static void clientSetup(FMLClientSetupEvent event)
    {
    }

    @SubscribeEvent
    public static void onModelBake(ModelBakeEvent event)
    {
        AVAClientUtil.replaceModel(event, ModItems.ASSEMBLE_GUN);
        if (ModItems.ASSEMBLE_GUN instanceof ICustomModel)
        {
            ModelResourceLocation loc = new ModelResourceLocation(ModItems.ASSEMBLE_GUN.getRegistryName() + "#inventory");
            IBakedModel origin = event.getModelRegistry().get(loc);
            IBakedModel newModel = ((ICustomModel) ModItems.ASSEMBLE_GUN).getCustomModel(origin);
            if (origin != null && newModel != null)
                event.getModelRegistry().put(loc, newModel);
        }
        ModelResourceLocation loc = new ModelResourceLocation(ModItems.TEST_ITEM.getRegistryName() + "#inventory");
        IBakedModel origin = event.getModelRegistry().get(loc);
        IBakedModel newModel = new Model(origin, TestModel::new);
        if (origin != null)
            event.getModelRegistry().put(loc, newModel);
    }

    @SubscribeEvent
    public static void onModelRegister(ModelRegistryEvent event)
    {
        for (GunComponents.GunComponentModels model : GunComponents.GunComponentModels.values())
            ModelLoader.addSpecialModel(model.getModelLocation());
        AssembleGunModel.addSpecialModels();
    }

    public static final ResourceLocation BLOCK_ATLAS = new ResourceLocation("minecraft:textures/atlas/blocks.png");
    @SubscribeEvent
    public static void onTexturesStitch(TextureStitchEvent.Pre event)
    {
        if (event.getMap().getTextureLocation().equals(BLOCK_ATLAS))
            for (ColourMessage.Colour colour : ColourMessage.Colour.values())
                event.addSprite(colour.getLocation());
    }
}
