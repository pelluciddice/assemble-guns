package dice.assembleguns.events;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import dice.assembleguns.component.AssembleGunModel;
import dice.assembleguns.component.GunParts;
import dice.assembleguns.component.ModificationScreen;
import dice.assembleguns.component.components.GunComponents;
import dice.assembleguns.component.components.GunComponentsManager;
import dice.assembleguns.component.components.parts.GunPart;
import dice.assembleguns.items.AssembleGunItem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.vector.Matrix4f;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraft.world.LightType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import pellucid.ava.events.forge.AVARenderHandEvent;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.misc.AVAWeaponUtil;
import pellucid.ava.misc.cap.WorldData;
import pellucid.ava.misc.config.AVAClientConfig;

import static pellucid.ava.util.AVAClientUtil.drawTransparent;
import static pellucid.ava.util.AVACommonUtil.*;

@Mod.EventBusSubscriber(Dist.CLIENT)
public class ClientForgeEventBus
{
    public static final ResourceLocation CROSSHAIR = new ResourceLocation("assemble_guns:textures/overlay/crosshair.png");
    @SubscribeEvent
    public static void onRenderHUD(RenderGameOverlayEvent.Pre event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = minecraft.player;
        if (player == null || !player.isAlive())
            return;
        int screenWidth = event.getWindow().getScaledWidth();
        int screenHeight = event.getWindow().getScaledHeight();
        ItemStack item = getHeldStack(player);
        MatrixStack stack = event.getMatrixStack();
        if (event.getType() == RenderGameOverlayEvent.ElementType.CROSSHAIRS && isAvailable(player) && minecraft.isGameFocused() && minecraft.gameSettings.getPointOfView().func_243192_a() && minecraft.currentScreen == null)
        {
            if (item.getItem() instanceof AssembleGunItem && !player.isSprinting() && getGun(player).initTags(getHeldStack(player)).getInt("reload") == 0 && WorldData.getCap(player.getEntityWorld()).shouldRenderCrosshair() && AVAClientConfig.SHOULD_RENDER_CROSS_HAIR.get())
            {
                event.setCanceled(true);
                GunPart part = GunComponentsManager.getCap(item).getKeys().get(GunParts.RAIL_LEFT.name());
                part.getComponentItem().ifPresent((comp) -> {
                    float reach = 30.0F;
                    RayTraceResult result = AVAWeaponUtil.rayTrace(player, reach, true);
                    if (result.getType() != RayTraceResult.Type.MISS)
                    {
                        minecraft.getTextureManager().bindTexture(CROSSHAIR);

                        stack.push();

                        float transparency = 0.5F;
                        if (result.getType() == RayTraceResult.Type.BLOCK)
                        {
                            BlockRayTraceResult blockResult = (BlockRayTraceResult) result;
                            Direction direction = blockResult.getFace();
                            stack.translate(screenWidth / 2.0F, screenHeight / 2.0F, 0.0F);
                            if (direction.getAxis() != Direction.Axis.Y)
                                stack.rotate(Vector3f.YP.rotationDegrees((player.rotationYaw % 360) - direction.getOpposite().getHorizontalIndex() * 90));
                            else
                                stack.rotate(Vector3f.XP.rotationDegrees(player.rotationPitch + 90 - direction.getIndex() * 180));
                            stack.translate(-screenWidth / 2.0F, -screenHeight / 2.0F, 0.0F);

                            BlockPos pos = vecToPos(blockResult.getHitVec());
                            int light = player.world.getLightFor(LightType.SKY, pos) + player.world.getLightFor(LightType.BLOCK, pos);
//                            int light2 = player.world.getLightFor(LightType.SKY, player.getPosition()) + player.world.getLightFor(LightType.BLOCK, player.getPosition());
                            //                            transparency = Math.min(Math.max((light2 - light) / 2.0F, 0.15F), 0.65F);
                            transparency = Math.min(Math.max(1.0F / light, 0.2F), 0.85F);
                        }

                        double distance = result.getHitVec().distanceTo(AVAWeaponUtil.getEyePositionFor(player));
                        float scale = (float) (reach - distance) - 10.5F;
                        stack.scale(scale, scale, scale);
                        drawTransparent(true);

                        if (comp.component == GunComponents.RED_LASER_LEFT)
                            RenderSystem.color4f(255.0F, 0.0F, 0.0F, transparency);
                        else if (comp.component == GunComponents.GREEN_LASER_LEFT)
                            RenderSystem.color4f(0.0F, 255.0F, 0.0F, transparency);
                        else
                            RenderSystem.color4f(0.0F, 0.0F, 255.0F, transparency);

                        float size = 0.5F;
                        RenderSystem.enableDepthTest();

                        Tessellator tessellator = Tessellator.getInstance();
                        BufferBuilder builder = tessellator.getBuffer();
                        builder.begin(7, DefaultVertexFormats.POSITION_TEX);

                        Matrix4f mat = stack.getLast().getMatrix();

                        float x1 = (screenWidth / scale - size) / 2.0F;
                        float x2 = x1 + size;

                        float y1 = (screenHeight / scale - size) / 2.0F;
                        float y2 = y1 + size;

                        builder.pos(mat, x1, y1, 0.0F).tex(0.0F, 1.0F).endVertex();
                        builder.pos(mat, x1, y2, 0.0F).tex(0.0F, 0.0F).endVertex();
                        builder.pos(mat, x2, y2, 0.0F).tex(1.0F, 0.0F).endVertex();
                        builder.pos(mat, x2, y1, 0.0F).tex(1.0F, 1.0F).endVertex();

                        tessellator.draw();
                        stack.pop();
                    }
                });
            }
        }
    }

    @SubscribeEvent
    public static void onHandRender(AVARenderHandEvent.Animation event)
    {
        MatrixStack stack = event.getMatrixStack();
        ItemStack item = event.getItemStack();
        AVAItemGun master = event.getMaster();
        if (item.isEmpty() || !(item.getItem() instanceof AssembleGunItem))
            return;
        switch (event.getType())
        {
            case RUN:
            case DRAW:
                break;
            case FIRE:
                event.addLeft(AssembleGunModel.getFireAnimationLeftHand(item));
                event.addRight(AssembleGunModel.getFireAnimationRightHand(item));
                break;
            case RELOAD:
                event.addLeft(AssembleGunModel.getReloadAnimationLeftHand(item));
                event.addRight(AssembleGunModel.getReloadAnimationRightHand(item));
                break;
        }
    }

    @SubscribeEvent
    public static void onHandRender(AVARenderHandEvent.Transformation event)
    {
        ItemStack item = event.getItemStack();
        AVAItemGun master = event.getMaster();
        if (!(master instanceof AssembleGunItem) || item.isEmpty() || !(item.getItem() instanceof AssembleGunItem))
            return;
        if (event.getType() == AVARenderHandEvent.Type.IDLE_LEFT)
            event.addPerspective(AssembleGunModel.getLeftHandIdle(item));
    }

    public static void openModificationScreen()
    {
        Minecraft.getInstance().displayGuiScreen(new ModificationScreen());
    }
}
