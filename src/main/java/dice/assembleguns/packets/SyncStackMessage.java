package dice.assembleguns.packets;

import dice.assembleguns.component.ModificationScreen;
import dice.assembleguns.component.components.GunComponents;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.function.Supplier;

public class SyncStackMessage
{
    private final CompoundNBT compound;
    public SyncStackMessage(ItemStack stack)
    {
        this(stack.getOrCreateTag());
    }

    public SyncStackMessage(CompoundNBT compound)
    {
        this.compound = compound;
    }

    public static void encode(SyncStackMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeCompoundTag(msg.compound);
    }

    public static SyncStackMessage decode(PacketBuffer packetBuffer)
    {
        return new SyncStackMessage(packetBuffer.readCompoundTag());
    }

    public static void handle(SyncStackMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
            DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> handle(message.compound));
        });
        ctx.get().setPacketHandled(true);
    }

    @OnlyIn(Dist.CLIENT)
    private static void handle(CompoundNBT compound)
    {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = minecraft.player;
        if (player != null)
        {
            player.getHeldItemMainhand().setTag(compound);
            if (minecraft.currentScreen instanceof ModificationScreen)
                ((ModificationScreen) minecraft.currentScreen).refreshOptions();
        }
    }
}
