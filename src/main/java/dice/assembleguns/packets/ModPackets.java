package dice.assembleguns.packets;

import dice.assembleguns.AssembleGuns;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

public class ModPackets
{
    private static final String PROTOCOL_VERSION = "1";
    public static final SimpleChannel INSTANCE = NetworkRegistry.newSimpleChannel(new ResourceLocation(AssembleGuns.MODID, "main"),
            () -> PROTOCOL_VERSION, PROTOCOL_VERSION::equals, PROTOCOL_VERSION::equals
    );

    public static void registerAll()
    {
        int id = 0;
        INSTANCE.registerMessage(id++, ModifyMessage.class, ModifyMessage::encode, ModifyMessage::decode, ModifyMessage::handle);
        INSTANCE.registerMessage(id++, SyncStackMessage.class, SyncStackMessage::encode, SyncStackMessage::decode, SyncStackMessage::handle);
        INSTANCE.registerMessage(id++, ColourMessage.class, ColourMessage::encode, ColourMessage::decode, ColourMessage::handle);
    }
}
