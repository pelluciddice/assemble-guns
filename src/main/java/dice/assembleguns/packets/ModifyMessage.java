package dice.assembleguns.packets;

import dice.assembleguns.component.GunParts;
import dice.assembleguns.component.components.GunComponents;
import dice.assembleguns.component.components.GunComponentsManager;
import dice.assembleguns.items.AssembleGunItem;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvents;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class ModifyMessage
{
    public static final int UNINSTALL = 0;
    public static final int INSTALL = 1;
    private final GunComponents toModify;
    private final int type;
    public ModifyMessage(GunComponents toModify, int type)
    {
        this.toModify = toModify;
        this.type = type;
    }

    public static void encode(ModifyMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeEnumValue(msg.toModify).writeInt(msg.type);
    }

    public static ModifyMessage decode(PacketBuffer packetBuffer)
    {
        return new ModifyMessage(packetBuffer.readEnumValue(GunComponents.class), packetBuffer.readInt());
    }

    public static void handle(ModifyMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
            ServerPlayerEntity player = ctx.get().getSender();
            if (player != null)
            {
                if (performModification(player, player.getHeldItemMainhand(), message.toModify, message.type))
                    ModPackets.INSTANCE.sendTo(new SyncStackMessage(player.getHeldItemMainhand()), player.connection.getNetworkManager(), NetworkDirection.PLAY_TO_CLIENT);
            }
        });
        ctx.get().setPacketHandled(true);
    }

    public static boolean performModification(PlayerEntity player, ItemStack stack, GunComponents component, int type)
    {
        if (stack.getItem() instanceof AssembleGunItem)
        {
            boolean modified = false;
            GunComponentsManager manager = GunComponentsManager.getCap(stack);
            if (type == UNINSTALL)
            {
                manager.unInstall(component);
                modified = true;
                stack.setDamage((int) (stack.getDamage() + component.getAttributesMap().get(GunComponents.ComponentAttributes.DURABILITY)));
                if (stack.getDamage() > stack.getMaxDamage())
                    stack.setDamage(stack.getMaxDamage());
            }
            else if (craft(player, component.getCost()))
            {
                manager.install(component);
                modified = true;
                stack.setDamage((int) (stack.getDamage() - component.getAttributesMap().get(GunComponents.ComponentAttributes.DURABILITY)));
            }
            if (modified)
            {
                if (!GunComponents.ComponentAttributes.CAPACITY.isDefault(component.getAttributesMap().get(GunComponents.ComponentAttributes.CAPACITY)))
                    stack.getOrCreateTag().putInt("ammo", 0);
                player.world.playSound(null, player.getPosX(), player.getPosY(), player.getPosZ(), SoundEvents.BLOCK_ANVIL_USE, SoundCategory.PLAYERS, 1.0F, 1.0F);
            }
            return modified;
        }
        return false;
    }

    private static boolean craft(PlayerEntity player, int cost)
    {
        if (player.abilities.isCreativeMode)
            return true;
        List<ItemStack> stacks = new ArrayList<>();
        int total = 0;
        for (int i=0;i<player.inventory.getSizeInventory();i++)
        {
            ItemStack stack = player.inventory.getStackInSlot(i);
            if (stack.getItem() == Items.IRON_INGOT)
            {
                stacks.add(stack);
                total += stack.getCount();
            }
        }
        if (total >= cost)
        {
            for (ItemStack stack : stacks)
            {
                int taken = Math.min(stack.getCount(), cost);
                stack.shrink(taken);
                cost -= taken;
                if (cost <= 0)
                    return true;
            }
        }
        return false;
    }
}
