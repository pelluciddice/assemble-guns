package dice.assembleguns.packets;

import dice.assembleguns.AssembleGuns;
import dice.assembleguns.component.AssembleGunModel;
import dice.assembleguns.component.components.GunComponents;
import dice.assembleguns.events.ClientModEventBus;
import dice.assembleguns.items.AssembleGunItem;
import net.minecraft.client.renderer.model.RenderMaterial;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;

import java.util.Locale;
import java.util.function.Supplier;

public class ColourMessage
{
    protected final int hex;
    public ColourMessage(int hex)
    {
        this.hex = hex;
    }

    public static void encode(ColourMessage msg, PacketBuffer packetBuffer)
    {
        packetBuffer.writeInt(msg.hex);
    }

    public static ColourMessage decode(PacketBuffer packetBuffer)
    {
        return new ColourMessage(packetBuffer.readInt());
    }

    public static void handle(ColourMessage message, Supplier<NetworkEvent.Context> ctx)
    {
        ctx.get().enqueueWork(() -> {
            ServerPlayerEntity player = ctx.get().getSender();
            if (player != null)
            {
                ItemStack stack = player.getHeldItemMainhand();
                if (stack.getItem() instanceof AssembleGunItem)
                    stack.getOrCreateTag().putInt("colour", message.hex);
            }
        });
        ctx.get().setPacketHandled(true);
    }

    public enum Colour
    {
        BLACK_WHITE,
        BLACK_RED;

        private ResourceLocation location = null;
        private ResourceLocation textureLocation = null;
        private RenderMaterial material = null;
        Colour()
        {

        }

        public ResourceLocation getLocation()
        {
            if (location == null)
                location = new ResourceLocation(AssembleGuns.MODID, "item/" + name().toLowerCase(Locale.ROOT));
            return location;
        }

        public ResourceLocation getTextureLocation()
        {
            if (textureLocation == null)
                textureLocation = new ResourceLocation(AssembleGuns.MODID, "textures/item/" + name().toLowerCase(Locale.ROOT));
            return textureLocation;
        }

        public RenderMaterial getMaterial()
        {
            if (material == null)
                material = new RenderMaterial(ClientModEventBus.BLOCK_ATLAS, getLocation());
            return material;
        }
    }
}
