package dice.assembleguns.items;

import dice.assembleguns.component.GunParts;
import dice.assembleguns.component.components.GunComponents;
import dice.assembleguns.component.components.GunComponentsManager;
import dice.assembleguns.events.ClientForgeEventBus;
import net.minecraft.block.AnvilBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.NonNullList;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.fml.DistExecutor;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.AmmoKit;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.misc.AVASounds;
import pellucid.ava.misc.AVAWeaponUtil;
import pellucid.ava.misc.cap.IPlayerAction;
import pellucid.ava.misc.cap.PlayerAction;
import pellucid.ava.util.AVAClientUtil;
import pellucid.ava.util.AVACommonUtil;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Optional;

public class AssembleGunItem extends AVAItemGun
{
    public AssembleGunItem()
    {
        super(new Properties(AVAWeaponUtil.Classification.EXCLUDED).range(45).accuracy(25).initialAccuracy(10).stability(35).mobility(100).magazineType(ModItems.AMMO_BAG), new Recipe().addItem(Items.BARRIER), ModItems.MAIN);
    }

    @Override
    public void reload(CompoundNBT compound, LivingEntity shooter, ItemStack stack)
    {
        boolean isPlayer = shooter instanceof PlayerEntity;
        while ((!isPlayer || (getSlotForMagazine((PlayerEntity) shooter, stack) != -1 || ((PlayerEntity) shooter).abilities.isCreativeMode)) && compound.getInt("ammo") < getMaxAmmo(stack) && (!isReloadInteractable(stack) || compound.getInt("interact") == 0))
        {
            int have = compound.getInt("ammo");
            final int need = getMaxAmmo(stack) - have;
            int provided = need;
            if (isPlayer && !((PlayerEntity) shooter).abilities.isCreativeMode)
            {
                int slot = getSlotForMagazine((PlayerEntity) shooter, stack);
                if (slot == -1)
                    break;
                ItemStack mag = ((PlayerEntity) shooter).inventory.getStackInSlot(slot);
                if ((mag.getItem() instanceof AmmoKit && ((AmmoKit) mag.getItem()).refillRequired) || mag.getItem() instanceof AmmoBag)
                {
                    provided = Math.min(need, mag.getMaxDamage() - mag.getDamage());
                    mag.setDamage(mag.getDamage() + provided);
                    compound.putInt("ammo", provided + have);
                    break;
                }
            }
            compound.putInt("ammo", provided + have);
        }
        compound.putInt("reload", 0);
    }

    @Override
    protected int getSlotForMagazine(PlayerEntity player, ItemStack stack)
    {
        for (int slot=0;slot<player.inventory.getSizeInventory();slot++)
        {
            ItemStack slotStack = player.inventory.getStackInSlot(slot);
            if (slotStack.getItem() == getMagazineType(stack) && slotStack.getDamage() < slotStack.getMaxDamage())
                return slot;
        }
        return -1;
    }

    @Override
    public ActionResultType onItemUseFirst(ItemStack stack, ItemUseContext context)
    {
        DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
            if (context.getWorld().getBlockState(context.getPos()).getBlock() instanceof AnvilBlock && context.getPlayer() != null && context.getPlayer().isSneaking())
                ClientForgeEventBus.openModificationScreen();
        });
        return ActionResultType.CONSUME;
    }

    @Override
    public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        CompoundNBT compound = initTags(stack);
        if (!entityIn.isAlive() || !(entityIn instanceof PlayerEntity))
            return;
        PlayerEntity player = (PlayerEntity) entityIn;
        IPlayerAction capability = PlayerAction.getCap(player);
        if (!isSelected)
        {
            compound.putInt("reload", 0);
            compound.putInt("run", 0);
            compound.putInt("draw", 0);
            compound.putInt("ticks", 0);
            compound.putInt("fire", 0);
        }
        else
        {
            if (player.isSprinting() && player.isOnGround() && !player.isSneaking() && compound.getInt("run") == 0 && compound.getInt("reload") == 0 && !capability.isFiring())
                compound.putInt("run", 1);
            else if ((!player.isSprinting() || !player.isOnGround() || player.isSneaking() || compound.getInt("reload") != 0 || capability.isFiring()) && compound.getInt("run") != 0)
                compound.putInt("run", 0);
            if (compound.getInt("run") > 0)
            {
                if (compound.getInt("run") >= 12)
                    compound.putInt("run", 1);
                else
                    compound.putInt("run", compound.getInt("run") + 1);
            }
        }
        if (compound.getInt("ticks") > 0)
            compound.putInt("ticks", compound.getInt("ticks") - 1);
        if (compound.getInt("fire") > 0)
        {
            compound.putInt("fire", compound.getInt("fire") + 1);
            if (compound.getInt("fire") >= getFireAnimation(stack))
                compound.putInt("fire", 0);
        }
        if (compound.getInt("interact") > 0)
        {
            compound.putInt("reload", 0);
            compound.putInt("interact", compound.getInt("interact") + 1);
            if (compound.getInt("interact") >= 2)
                compound.putInt("interact", 0);
        }
        if (compound.getInt("reload") > 0)
        {
            compound.putInt("reload", compound.getInt("reload") + 1);
            if (compound.getInt("reload") >= getReloadTime(stack))
                reload(compound, (LivingEntity) entityIn, stack);
        }
    }

    @Override
    public CompoundNBT initTags(ItemStack stack)
    {
        CompoundNBT compound;
        if (stack.hasTag())
            compound = stack.getTag();
        else
        {
            compound = new CompoundNBT();
            compound.putInt("ammo", 0);
            compound.putInt("fire", 0);
            compound.putInt("interact", 0);
            compound.putInt("run", 0);
            compound.putInt("ticks", 0);
            compound.putInt("reload", 0);
            compound.putInt("draw", 0);
            compound.putInt("colour", Integer.valueOf("FFFFFF", 16));
            stack.setTag(compound);
        }

        return compound;
    }

    @Override
    public boolean getIsRepairable(ItemStack toRepair, ItemStack repair)
    {
        return repair.getItem() == Items.IRON_INGOT;
    }

    @Override
    public void addCustomToolTips(ItemTooltipEvent event)
    {
        GunComponents.ComponentAttributes.getVisibleAttributes().forEach((attribute) -> addTotalWithColour(event.getToolTip(), event.getItemStack(), attribute));
        event.getToolTip().add(AVAClientUtil.applyStyleForPercentage(new StringTextComponent("Maintenance: "), getMaintenance(event.getItemStack()), 0.3F, 0.8F, 1.0F));
    }

    private static float roundNoFloor(float num)
    {
        return (float) AVACommonUtil.roundAndMin(num, 4, Integer.MIN_VALUE);
    }


    private static float round(float num)
    {
        return (float) AVACommonUtil.roundAndMin(num, 4, 0);
    }

    @Override
    public float getBulletDamage(ItemStack stack, boolean modified)
    {
        return round(getTotal(stack, GunComponents.ComponentAttributes.ATTACK_DAMAGE, true));
    }

    @Override
    public float getStability(ItemStack stack, boolean modified)
    {
        return roundNoFloor(super.getStability(stack, modified) + getTotalModified(stack, GunComponents.ComponentAttributes.STABILITY, modified, true) / 2.5F);
    }

    @Override
    public float getInitialAccuracy(ItemStack stack)
    {
        return roundNoFloor(super.getInitialAccuracy(stack) + getTotalModified(stack, GunComponents.ComponentAttributes.INITIAL_ACCURACY, true, true) / 20.0F);
    }

    @Override
    public float getAccuracy(ItemStack stack, boolean modified)
    {
        return roundNoFloor(super.getAccuracy(stack, modified) + getTotalModified(stack, GunComponents.ComponentAttributes.ACCURACY, modified, true) / 7F);
    }

    @Override
    public int getRange(ItemStack stack, boolean modified)
    {
        return super.getRange(stack, modified) + (int) getTotal(stack, GunComponents.ComponentAttributes.RANGE, true);
    }

    @Override
    public float getAttackSpeed(ItemStack stack, boolean modified)
    {
        return round(getTotalModified(stack, GunComponents.ComponentAttributes.FIRE_RATE, false));
    }

    @Override
    public int getFireAnimation(ItemStack stack)
    {
        return manager(stack).isInstalled(GunComponents.CHAMBER_MANUAL) ? (int) getAttackSpeed(stack, false) : 2;
    }

    @Override
    protected float getFireVolume(ItemStack stack)
    {
        return 1.0F + getTotal(stack, GunComponents.ComponentAttributes.VOLUME);
    }

    @Override
    protected float getFirePitch(ItemStack stack)
    {
        return 1.0F + getTotal(stack, GunComponents.ComponentAttributes.PITCH);
    }

    @Override
    public float getMobility(ItemStack stack)
    {
        return round(super.getMobility(stack) + getTotal(stack, GunComponents.ComponentAttributes.WEIGHT));
    }

    @Override
    public int getMaxAmmo(ItemStack stack)
    {
        return (int) getTotal(stack, GunComponents.ComponentAttributes.CAPACITY);
    }

    @Override
    public int getReloadTime(ItemStack stack)
    {
        Optional<GunComponentItem> component = manager(stack).getPart(GunParts.CHAMBER).getComponentItem();
        if (component.isPresent())
        {
            GunComponents chamber = component.get().component;
            if (chamber == GunComponents.CHAMBER_AUTOMATIC)
                return 30;
            return 42;
        }
        return 0;
    }

    @Override
    public IScopeType getScopeType(ItemStack stack)
    {
        return GunComponents.ScopeTypes.EMPTY;
    }

    @Override
    public boolean isAuto(ItemStack stack)
    {
        return manager(stack).isInstalled(GunComponents.CHAMBER_AUTOMATIC);
    }

    @Override
    public boolean firable(LivingEntity shooter, ItemStack stack)
    {
        if (shooter instanceof PlayerEntity && stack.getDamage() >= stack.getMaxDamage())
            return false;
        return super.firable(shooter, stack);
    }

    @Override
    public boolean fire(World world, LivingEntity shooter, @Nullable LivingEntity target, ItemStack stack, AVACommonUtil.QuadConsumer<IPlayerAction, PlayerEntity, ItemStack, Float> recoil, AVACommonUtil.QuadConsumer<IPlayerAction, PlayerEntity, ItemStack, Float> shake)
    {
        boolean fired = super.fire(world, shooter, target, stack, recoil, shake);
        float num = random.nextFloat();
        if (fired && num < 0.2F && !world.isRemote())
            stack.setDamage(stack.getDamage() + 1);
        return fired;
    }

    @Override
    public int getMaxDamage(ItemStack stack)
    {
        return (int) getTotal(stack, GunComponents.ComponentAttributes.DURABILITY);
    }

    @Override
    protected void summonBullet(World world, LivingEntity shooter, ItemStack stack, @Nullable LivingEntity target, float spread)
    {
        for (int i=0;i<getBulletsPerShot(stack) + 1;i++)
            super.summonBullet(world, shooter, stack, target, spread);
    }

    public static float getMaintenance(ItemStack stack)
    {
        return (float) AVACommonUtil.roundAndMin((stack.getMaxDamage() - stack.getDamage()) / (double) stack.getMaxDamage(), 2, 0);
    }

    public int getBulletsPerShot(ItemStack stack)
    {
        return (int) getTotal(stack, GunComponents.ComponentAttributes.BULLETS_PER_SHOT);
    }

    public static void addTotalWithColour(List<ITextComponent> list, ItemStack stack, GunComponents.ComponentAttributes attribute)
    {
        if (!attribute.isDefault(getTotal(stack, attribute)))
            list.add(getTotalWithColour(stack, attribute));
    }

    public static ITextComponent getTotalWithColour(ItemStack stack, GunComponents.ComponentAttributes attribute)
    {
        float value = roundNoFloor(getTotal(stack, attribute));
        return new StringTextComponent(AVACommonUtil.toDisplayString(attribute.name()) + ": ").appendSibling(new StringTextComponent(String.valueOf(value)).setStyle(Style.EMPTY.applyFormatting(attribute.isBeneficial(value) ? TextFormatting.GREEN : TextFormatting.RED)));
    }

    public static float getTotalModified(ItemStack stack, GunComponents.ComponentAttributes attribute, boolean modified)
    {
        return getTotalModified(stack, attribute, modified, false);
    }

    public static float getTotalModified(ItemStack stack, GunComponents.ComponentAttributes attribute, boolean modified, boolean withMaintenance)
    {
        return getTotal(stack, attribute, withMaintenance) * (modified ? -1 : 1);
    }

    public static float getTotal(ItemStack stack, GunComponents.ComponentAttributes attribute)
    {
       return getTotal(stack, attribute, false);
    }

    public static float getTotal(ItemStack stack, GunComponents.ComponentAttributes attribute, boolean withMaintenance)
    {
        if (stack.getItem() instanceof AssembleGunItem)
            return manager(stack).sum(attribute) * (withMaintenance ? getMaintenance(stack) : 1.0F);
        return 0.0F;
    }

    protected static GunComponentsManager manager(ItemStack stack)
    {
        return GunComponentsManager.getCap(stack);
    }

    @Override
    public SoundEvent getShootSound(ItemStack stack)
    {
        GunComponentsManager manager = manager(stack);
        if (manager.isInstalled(GunComponents.CHAMBER_AUTOMATIC))
        {
            if (manager.isPartInstalled(GunParts.HANDGUARD))
            {
                if (GunComponents.isLongFront(manager.front.getComponentItem().get().component))
                    return AVASounds.AK12_SHOOT;
                return AVASounds.M4A1_SHOOT;
            }
            return AVASounds.SW1911_SHOOT;
        }
        else if (manager.isInstalled(GunComponents.CHAMBER_SEMI_AUTOMATIC))
        {
            if (manager.isPartInstalled(GunParts.HANDGUARD))
            {
                if (GunComponents.isLongFront(manager.front.getComponentItem().get().component))
                    return AVASounds.MK20_SHOOT;
                return AVASounds.FG42_SHOOT;
            }
            return AVASounds.FN_FNC_SHOOT;
        }
        if (manager.front.installed() && GunComponents.isLongFront(manager.front.getComponentItem().get().component))
            return AVASounds.M24_SHOOT;
        return AVASounds.MOSIN_NAGANT_SHOOT;
    }

    @Override
    public SoundEvent getReloadSound(ItemStack stack)
    {
        GunComponentsManager manager = manager(stack);
        if (manager.isInstalled(GunComponents.CHAMBER_AUTOMATIC))
            return AVASounds.MP5SD5_RELOAD;
        else if (manager.isInstalled(GunComponents.CHAMBER_SEMI_AUTOMATIC))
            return AVASounds.M4A1_RELOAD;
        return AVASounds.MK20_RELOAD;
    }

    @Nullable
    @Override
    public ICapabilityProvider initCapabilities(ItemStack stack, @Nullable CompoundNBT nbt)
    {
        return new GunComponentsManager();
    }

    @Override
    public CompoundNBT getShareTag(ItemStack stack)
    {
        CompoundNBT compound = stack.getOrCreateTag();
        compound.put("manager", GunComponentsManager.getCap(stack).serializeNBT());
        return compound;
    }

    @Override
    public void readShareTag(ItemStack stack, CompoundNBT nbt)
    {
        if (nbt != null && nbt.contains("manager"))
            GunComponentsManager.getCap(stack).deserializeNBT(nbt.getCompound("manager"));
        super.readShareTag(stack, nbt);
    }

    @Override
    public void fillItemGroup(ItemGroup group, NonNullList<ItemStack> items)
    {
        if (isInGroup(group))
            items.add(new ItemStack(this));
    }
}
