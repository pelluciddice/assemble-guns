package dice.assembleguns.items;

import dice.assembleguns.component.components.GunComponents;
import net.minecraft.item.Item;

public class GunComponentItem extends Item
{
    public final GunComponents component;

    public GunComponentItem(GunComponents component)
    {
        super(new Properties().maxStackSize(1));
        this.component = component;
    }
}
