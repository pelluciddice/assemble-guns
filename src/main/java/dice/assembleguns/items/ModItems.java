package dice.assembleguns.items;

import dice.assembleguns.component.AssembleGunModel;
import dice.assembleguns.component.components.GunComponents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.registries.IForgeRegistry;
import pellucid.ava.items.guns.AVAItemGun;
import pellucid.ava.items.miscs.Ammo;

import java.util.Locale;

public class ModItems
{
    public static Item AMMO_BAG;
    public static Item ASSEMBLE_GUN;
    public static Item TEST_ITEM;

    public static void registerAll(IForgeRegistry<Item> registry)
    {
        for (GunComponents component : GunComponents.values())
            registry.register(new GunComponentItem(component).setRegistryName(component.name().toLowerCase(Locale.ROOT)));
        registry.registerAll(
                AMMO_BAG = new AmmoBag().setRegistryName("ammo_bag"),
                ASSEMBLE_GUN = new AssembleGunItem().setRegistryName("assemble_gun"),
                TEST_ITEM = new Item(new Item.Properties()).setRegistryName("test_item")
        );
        Ammo.AMMO.remove(AMMO_BAG);
        DistExecutor.runWhenOn(Dist.CLIENT, () -> ModItems::setModels);
    }

    @OnlyIn(Dist.CLIENT)
    public static void setModels()
    {
        ((AVAItemGun) ASSEMBLE_GUN).setCustomModel(AssembleGunModel::new);
    }

    public static final ItemGroup MAIN = new ItemGroup("main")
    {
        @OnlyIn(Dist.CLIENT)
        public ItemStack createIcon()
        {
            return new ItemStack(AMMO_BAG);
        }
    };
}
