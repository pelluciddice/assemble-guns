package dice.assembleguns.items;

import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.World;
import pellucid.ava.items.miscs.Ammo;
import pellucid.ava.items.miscs.Recipe;
import pellucid.ava.util.AVAClientUtil;

import javax.annotation.Nullable;
import java.util.List;

public class AmmoBag extends Ammo
{
    public AmmoBag()
    {
        super(new Properties().group(ModItems.MAIN).maxStackSize(1).maxDamage(1000), new Recipe().addItem(Items.BARRIER), true);
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
    {
        tooltip.add(AVAClientUtil.applyStyleForPercentage(new StringTextComponent("Ammo: "), stack.getMaxDamage() - stack.getDamage(), 300, 700, 1000));
    }

    @Override
    public boolean getIsRepairable(ItemStack toRepair, ItemStack repair)
    {
        return repair.getItem() == Items.IRON_BLOCK;
    }

    @Override
    public ItemStack addToInventory(PlayerEntity player, int times, boolean simulate)
    {
        for (int slot = 0;slot<player.inventory.getSizeInventory();slot++)
        {
            ItemStack stack = player.inventory.getStackInSlot(slot);
            if (stack.getItem() == ModItems.AMMO_BAG)
            {
                System.out.println("sett");
                stack.setDamage(stack.getDamage() - Math.min(stack.getDamage(), 150));
                break;
            }
        }
        return ItemStack.EMPTY;
    }

    @Override
    public int getItemStackLimit(ItemStack stack)
    {
        return 1;
    }
}
